<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/about', 'HomeController@about');

Route::get('/contact', 'HomeController@contact');

Route::middleware(['auth'])->group(function () {
    Route::get('/checkout', 'OrderController@checkout')->name('checkout');
    Route::post('/checkout', 'OrderController@create')->name('checkout.order');
    Route::post('/addtocart', 'OrderController@addToCart')->name('add_to_cart');
});

Route::get('workshop/', 'CourseController@showAll')->name('course');
Route::get('workshop/{slug}-{id}.html', 'CourseController@detail')->where('slug','^([0-9A-Za-z\-]+)?')->name('course_detail');

Route::get('educator/', 'CoachController@showAll')->name('coach');
Route::get('educator/{slug}-{id}.html', 'CoachController@detail')->where('slug','^([0-9A-Za-z\-]+)?')->name('coach_detail');

Route::get('blog/', 'BlogController@showAll')->name('blog');
Route::get('blog/{slug}-{id}.html', 'BlogController@detail')->where('slug','^([0-9A-Za-z\-]+)?')->name('blog_detail');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::prefix('dashboard')->group(function () {
        Route::get('/', function(){
            if (Auth::user()->user_type_id == 1) {
                return redirect()->route('admin');
            } else if (Auth::user()->user_type_id == 3) {
                return redirect()->route('admin_educator');
            }
            return redirect()->route('dashboard.orders');
        })->name('dashboard');
        
        Route::get('/order', 'DashboardController@index')->name('dashboard.orders');
        Route::get('/order/gets', 'DashboardController@getOrders')->name('dashboard.orders.gets');
        Route::get('/order/{id}', 'DashboardController@detail')->name('dashboard.order.detail');
        Route::post('/order/{id}/update', 'DashboardController@update')->name('dashboard.order.update');

        // change password
        Route::get('/change-password','DashboardController@changePasswordIndex')->name('dashboard.change_password');
        Route::post('/change-password','DashboardController@changePassword')->name('dashboard.change_password.submit');
    });

    Route::middleware('auth.educator.check')->prefix('educatordashboard')->group(function () {
        Route::get('/', function() {
            
        })->name('educatordashboard');
    });

    Route::middleware('auth.admin.check')->prefix('admin')->group(function () {
        Route::get('/', function(){
            return redirect()->route('admin.orders');
        })->name('admin');

        Route::get('/order', 'AdminController@orderIndex')->name('admin.orders');
        Route::get('/order/gets', 'AdminController@getOrders')->name('admin.orders.gets');
        Route::get('/order/{id}', 'AdminController@orderDetail')->name('admin.orders.detail');
        Route::post('/order/{id}/update', 'AdminController@orderUpdate')->name('admin.orders.update');

        Route::get('/course', 'AdminController@courseIndex')->name('admin.courses');
        Route::get('/course/add', 'AdminController@courseAddIndex')->name('admin.courses.add_index');
        Route::post('/course', 'AdminController@courseAdd')->name('admin.courses.add');
        Route::get('/course/gets', 'AdminController@getCourses')->name('admin.courses.gets');
        Route::get('/course/{id}', 'AdminController@courseDetail')->name('admin.courses.detail');
        Route::delete('/course/{id}', 'AdminController@courseDelete')->name('admin.courses.delete');
        Route::post('/course/{id}/update', 'AdminController@courseUpdate')->name('admin.courses.update');

        Route::get('/coach', 'AdminController@coachIndex')->name('admin.coaches');
        Route::get('/coach/add', 'AdminController@coachAddIndex')->name('admin.coaches.add_index');
        Route::post('/coach', 'AdminController@coachAdd')->name('admin.coaches.add');
        Route::get('/coach/gets', 'AdminController@getCoaches')->name('admin.coaches.gets');
        Route::get('/coach/{id}', 'AdminController@coachDetail')->name('admin.coaches.detail');
        Route::delete('/coach/{id}', 'AdminController@coachDelete')->name('admin.coaches.delete');
        Route::post('/coach/{id}/update', 'AdminController@coachUpdate')->name('admin.coaches.update');

        Route::get('/category', 'AdminController@categoryIndex')->name('admin.categories');
        Route::get('/category/add', 'AdminController@categoryAddIndex')->name('admin.categories.add_index');
        Route::post('/category', 'AdminController@categoryAdd')->name('admin.categories.add');
        Route::get('/category/gets', 'AdminController@getCategories')->name('admin.categories.gets');
        Route::get('/category/{id}', 'AdminController@categoryDetail')->name('admin.categories.detail');
        Route::delete('/category/{id}', 'AdminController@categoryDelete')->name('admin.categories.delete');
        Route::post('/category/{id}/update', 'AdminController@categoryUpdate')->name('admin.categories.update');

        Route::get('/banner', 'AdminController@bannerIndex')->name('admin.banners');
        Route::get('/banner/add', 'AdminController@bannerAddIndex')->name('admin.banners.add_index');
        Route::post('/banner', 'AdminController@bannerAdd')->name('admin.banners.add');
        Route::get('/banner/gets', 'AdminController@getbanners')->name('admin.banners.gets');
        Route::get('/banner/{id}', 'AdminController@bannerDetail')->name('admin.banners.detail');
        Route::delete('/banner/{id}', 'AdminController@bannerDelete')->name('admin.banners.delete');
        Route::post('/banner/{id}/update', 'AdminController@bannerUpdate')->name('admin.banners.update');

        Route::get('/blog', 'AdminController@blogIndex')->name('admin.blogs');
        Route::get('/blog/add', 'AdminController@blogAddIndex')->name('admin.blogs.add_index');
        Route::post('/blog', 'AdminController@blogAdd')->name('admin.blogs.add');
        Route::get('/blog/gets', 'AdminController@getblogs')->name('admin.blogs.gets');
        Route::get('/blog/{id}', 'AdminController@blogDetail')->name('admin.blogs.detail');
        Route::delete('/blog/{id}', 'AdminController@blogDelete')->name('admin.blogs.delete');
        Route::post('/blog/{id}/update', 'AdminController@blogUpdate')->name('admin.blogs.update');
    });
});