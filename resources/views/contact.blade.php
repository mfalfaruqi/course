@extends('layouts.general')
@section('page_name')
    Hubungi Kami
@endsection

@section('title')
    Hubungi Kami
@endsection

@section('content')
    <!-- Start of contact section
		============================================= -->
		@include('components.sosmed')
	<!-- End of contact section
		============================================= -->

	<!-- Start of contact area form
		============================================= -->
		@include('components.send_email')
	<!-- End of contact area form
		============================================= -->

	<!-- Start of contact area
		============================================= -->
		@include('components.contact_with_map')
	<!-- End of contact area
		============================================= -->
@endsection