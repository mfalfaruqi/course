@if (!empty($coaches))
<!-- Start secound teacher section
============================================= -->
<section id="teacher-2" class="secound-teacher-section">
    <div class="container">
        <div class="section-title mb35 headline text-left">
            <span class="subtitle ml42  text-uppercase">Educators</span>
            <h2>WGrow.id <span>Educators.</span></h2>
        </div>
        <div class="teacher-secound-slide">
            @foreach ($coaches as $coach)
            <div class="teacher-img-text relative-position text-center">
                <div class="teacher-img-social relative-position">
                    @if (!empty($coach->picture_url))
                        <a href="{{$coach->slug}}"><img src="{{Storage::url($coach->picture_url)}}" alt="{{$coach->name}}" style="display: block; width:200px; height:200px;"></a>
                    @else
                    <a href="{{$coach->slug}}"><img src="{{url('/')}}/img/teacher/default.jpg" alt="{{$coach->name}}"></a>
                    @endif
                </div>
                <div class="teacher-name-designation mt15">
                    <span class="teacher-name">{{$coach->name}}</span>
                    <span class="teacher-designation">{{$coach->highlight}}</span>
                </div>
            </div>
            @endforeach
        </div>

        <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
            <a href="{{route('coach')}}">Lihat Semua <i class="fas fa-caret-right"></i></a>
        </div>
    </div>
</section>
<!-- End teacher section
    ============================================= -->
@endif