<!-- The Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header backgroud-style">
                <div class="gradient-bg"></div>
                <div class="popup-logo">
                    <img src="{{url('/')}}/img/logo/p-logo.jpg" alt="">
                </div>
                <div class="popup-text text-center">
                    <h2> <span>Login</span></h2>
                    <p>Untuk pendaftaran, <a href="{{url('/')}}/register">Klik di sini.</a></p>
                </div>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form class="contact_form" action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="contact-info">
                        <input class="name" name="Email" type="email" placeholder="Your@email.com*">
                    </div>
                    <div class="contact-info">
                        <input class="pass" name="name" type="password" placeholder="Your password*">
                    </div>
                    <div class="nws-button text-center white text-capitalize">
                        <button type="submit" value="Submit">Login</button> 
                    </div>
                </form>
                <div class="log-in-footer text-center">
                    <p><a href="{{url('/')}}/password/reset">Lupa Password?</a></p>
                </div>
            </div>
        </div>
    </div>
</div>