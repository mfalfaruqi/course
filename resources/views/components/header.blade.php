<header>
    <div id="main-menu"  class="main-menu-container">
        <div  class="main-menu">
            <div class="container">
                <div class="navbar-default">
                    <div class="navbar-header float-left">
                        <a class="navbar-brand text-uppercase" href="/"><img src="{{url('/')}}/img/logo/new-logo-white.png" alt="logo" style="max-width:100%; max-height:40px;"></a>
                    </div><!-- /.navbar-header -->

                    <div class="cart-search float-right ul-li">
                        <ul>
                            {{-- <li>
                                <button type="button" class="toggle-overlay search-btn">
                                    <i class="fas fa-search"></i>
                                </button>
                                <div class="search-body">
                                    <div class="search-form">
                                        <form action="#">
                                            <input class="search-input" type="search" placeholder="Search Here">
                                            <div class="outer-close toggle-overlay">
                                                <button type="button" class="search-close">
                                                    <i class="fas fa-times"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li> --}}
                            <li><a href="{{route('checkout')}}"><i class="fas fa-shopping-bag"></i></a></li>
                            {{-- <span class="badge badge-danger">*</span> --}}
                        </ul>
                    </div>
                    <div class="log-in float-right">
                        @if (!Auth::check())
                            <a href="{{url('/login')}}" style="color:white;">log in</a>
                            {{-- <a data-toggle="modal" data-target="#modal-login" href="#">log in</a> --}}
                            @include('components.modal_login')
                        @endif
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <nav class="navbar-menu float-right">
                        <div class="nav-menu ul-li">
                            <ul>
                                <li class="menu-item-has-children ul-li-block">
                                    <a href="/">Home</a>
                                </li>
                                <li><a href="/about">Tentang Kami</a></li>
                                <li><a href="{{route('course')}}">Workshop</a></li>
                                <li><a href="{{route('coach')}}">Educator</a></li>
                                <li><a href="/blog">Artikel</a></li>
                                {{-- <li><a href="/contact">Hubungi Kami</a></li> --}}
                                @if (Auth::check())
                                <li class="menu-item-has-children ul-li-block">
                                    <a href="#">Akun Saya</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li><a href="{{route('dashboard.change_password')}}">Ganti Kata Sandi</a></li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}" 
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"
                                            >
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </nav>

                    <div class="mobile-menu">
                        <div class="logo"><a href="/"><img src="{{url('/')}}/img/logo/logo.png" alt="Logo"></a></div>
                        <nav>
                            <ul>
                                <li class="menu-item-has-children ul-li-block">
                                    <a href="/">Home</a>
                                </li>
                                <li><a href="/about">Tentang Kami</a></li>
                                <li><a href="/course">Workshop</a></li>
                                <li><a href="/coach">Educator</a></li>
                                <li><a href="/blog">Artikel</a></li>
                                {{-- <li><a href="/contact">Hubungi Kami</a></li> --}}
                                @if (Auth::check())
                                <li><a href="#">Akun Saya</a>
                                    <ul>
                                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li><a href="{{route('dashboard.change_password')}}">Ganti Kata Sandi</a></li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}" 
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"
                                            >
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>