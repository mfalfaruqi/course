<footer>
    <section id="footer-area" class="footer-area-section">
        <div class="container">
            <div class="footer-content pb10">
                <div class="row">
                    <div class="col-md-3">
                        <div class="footer-widget "  >
                            <div class="footer-logo mb35">
                                <img src="{{url('/')}}/img/logo/new-logo-black.png" alt="" style="max-width:220px; height:auto;">
                            </div>
                            <div class="footer-about-text">
                                <p><b>WGrow.id</b> - Your Growing Enabler.</p>
                                {{-- <p>Kami adalah platform yang bertujuan untuk meningkatkan kualitas kehidupan masyarakat indonesia melalui penyediaan akses program pengembangan diri yang tepat dan berkualitas.</p>
                                <p>Kami menjadi penghubung educator dengan user atau Perusahaan yang membutuhkan jasanya, membantu user menemukan program yang sesuai potensi dan kebutuhannya, meningkatkan kualitas educator sehingga mampu menjadi solusi di masyarakat</p> --}}

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="footer-widget">
                            <div class="footer-menu ul-li-block">
                                <h2 class="widget-title">Links</h2>
                                <ul>
                                    <li><a href="{{route('course')}}"><i class="fas fa-caret-right"></i>Workshop</a></li>
                                    <li><a href="{{route('coach')}}"><i class="fas fa-caret-right"></i>Educator</a></li>
                                    <li><a href="{{route('blog')}}"><i class="fas fa-caret-right"></i>Artikel</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer-menu ul-li-block "  >
                            <h2 class="widget-title">Account Info</h2>
                            <ul>
                                <li><a href="/login"><i class="fas fa-caret-right"></i>Login</a></li>
                                <li><a href="/register"><i class="fas fa-caret-right"></i>Register</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-social ul-li "  >
                            <h2 class="widget-title">Follow Us</h2>
                            <ul>
                                {{-- <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li> --}}
                                <li><a href="https://www.instagram.com/WGrow.id/" title="instagram"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <!-- <div class="col-md-3">
                        <div class="footer-widget "  >
                            <h2 class="widget-title">Photo Gallery</h2>
                            <div class="photo-list ul-li">
                                <ul>
                                    <li>
                                        <img src="{{url('/')}}/img/gallery/g-1.jpg" alt="">
                                        <div class="blakish-overlay"></div>
                                        <div class="pop-up-icon">
                                            <a href="{{url('/')}}/img/gallery/g-1.jpg" data-lightbox="roadtrip">
                                                <i class="fas fa-search"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="{{url('/')}}/img/gallery/g-2.jpg" alt="">
                                        <div class="blakish-overlay"></div>
                                        <div class="pop-up-icon">
                                            <a href="{{url('/')}}/img/gallery/g-2.jpg" data-lightbox="roadtrip">
                                                <i class="fas fa-search"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="{{url('/')}}/img/gallery/g-3.jpg" alt="">
                                        <div class="blakish-overlay"></div>
                                        <div class="pop-up-icon">
                                            <a href="{{url('/')}}/img/gallery/g-3.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="{{url('/')}}/img/gallery/g-4.jpg" alt="">
                                        <div class="blakish-overlay"></div>
                                        <div class="pop-up-icon">
                                            <a href="{{url('/')}}/img/gallery/g-4.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="{{url('/')}}/img/gallery/g-5.jpg" alt="">
                                        <div class="blakish-overlay"></div>
                                        <div class="pop-up-icon">
                                            <a href="{{url('/')}}/img/gallery/g-5.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="{{url('/')}}/img/gallery/g-6.jpg" alt="">
                                        <div class="blakish-overlay"></div>
                                        <div class="pop-up-icon">
                                            <a href="{{url('/')}}/img/gallery/g-6.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div> 
            <!-- /footer-widget-content -->
            {{-- <div class="footer-social-subscribe mb65">
                <div class="row">
                    <div class="col-md-3">
                        <div class="footer-social ul-li "  >
                            <h2 class="widget-title">Follow Us</h2>
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/WGrow.id/" title="instagram"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="subscribe-form "  >
                            <h2 class="widget-title">Subscribe Newsletter</h2>

                            <div class="subs-form relative-position">
                                <form action="#" method="post">
                                    <input class="course" name="course" type="email" placeholder="Email Address.">
                                    <div class="nws-button text-center  gradient-bg text-uppercase">
                                        <button type="submit" value="Submit">Subscribe now</button> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}

            <div class="copy-right-menu">
                <div class="row">
                    <div class="col-md-6">
                        <div class="copy-right-text">
                            <p>© 2019 - www.WGrow.id. All rights reserved</p>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="copy-right-menu-item float-right ul-li">
                            <ul>
                                <li><a href="#">License</a></li>
                                <li><a href="#">Privacy & Policy</a></li>
                                <li><a href="#">Term Of Service</a></li>
                            </ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
</footer>
