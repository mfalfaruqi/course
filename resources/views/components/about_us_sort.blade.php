<section id="about-us" class="about-us-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="bg-mockup">
                    <img src="img/about/abt.jpg" alt="">
                </div>
            </div>
            <!-- /form -->

            <div class="col-md-7">
                <div class="about-us-text">
                    <div class="section-title relative-position mb20 headline text-left "  >
                        <span class="subtitle ml42 text-uppercase">Tentang Kami</span>
                        <h2>We are <span>WGrow.id</span></h2>
                        <p>Visi Kami adalah untuk meningkatkan kualitas kehidupan masyarakat indonesia melalui penyediaan akses program pengembangan diri yang tepat dan berkualitas.</p>
                    </div>
                    <div class="about-content-text">
                            <p>Misi Kami adalah sebagai berikut.</p>
                        <div class="about-list mb65 ul-li-block "  >
                            <ul>
                                    <li>Menjadi penghubung educator dengan user atau perusahaan yang membutuhkan jasanya</li>
                                    <li>Membantu user menemukan program yang sesuai potensi dan kebutuhannya</li>
                                    <li>Meningkatkan kualitas educator sehingga mampu menjadi solusi di masyarakat</li>
                            </ul>
                        </div>
                        <div class="about-btn">
                            <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
                                <a href="/about">Lebih Lanjut <i class="fas fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

            {{-- <div class="col-md-7">
                <div class="about-us-text">
                    <div class="section-title relative-position mb20 headline text-left">
                        <span class="subtitle ml42 text-uppercase">Tentang Kami</span>
                        <h2>We are <span>WGrow.id</span></h2>
                        <p>Visi Kami adalah untuk meningkatkan kualitas kehidupan masyarakat indonesia melalui penyediaan akses program pengembangan diri yang tepat dan berkualitas.</p>
                    </div>
                    <div class="about-content-text">
                        <p>Misi Kami adalah sebagai berikut.</p>
                        <div class="about-list mb65 ul-li-block">
                            <ul>
                                <li>Menjadi penghubung educator dengan user atau perusahaan yang membutuhkan jasanya</li>
                                <li>Membantu user menemukan program yang sesuai potensi dan kebutuhannya</li>
                                <li>Meningkatkan kualitas educator sehingga mampu menjadi solusi di masyarakat</li>
                            </ul>
                        </div>
                        <div class="about-btn">
                            <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
                                <a href="/about">Lebih Lanjut <i class="fas fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
