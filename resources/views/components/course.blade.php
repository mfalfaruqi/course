@if (!empty($courses))
<section id="best-course" class="best-course-section" style="padding: 0px 0px;">
    <div class="container">
        <div class="section-title mb45 headline text-center">
            <span class="subtitle text-uppercase">WORKSHOP</span>
            <h2>WGrow.id<span> Workshop.</span></h2>
        </div>
        <div class="best-course-area mb45">
            <div class="row">
                @foreach ($courses as $course)
                    <div class="col-md-4">
                        <div class="best-course-pic-text relative-position">
                            <div class="best-course-pic relative-position">
                                <img src="{{Storage::url($course->banner_url)}}" alt="">
                                @if ($course->course_type_id == 1)
                                    <div class="course-price text-center gradient-bg" style="position:initial; border-radius:0px;">
                                        @if (!empty($course->fee))
                                            <span>Rp{{number_format($course->fee, 2, ',', '.')}}</span>
                                        @else
                                            <span>Gratis</span>
                                        @endif
                                    </div>
                                @else
                                    <div class="course-price text-center gradient-bg" style="position:initial; border-radius:0px;">
                                        <span>Contact Us For Detail</span>
                                    </div>
                                @endif
                                <div class="course-details-btn">
                                    <a href="{{$course->slug}}">DETAIL <i class="fas fa-arrow-right"></i></a>
                                </div>
                                <div class="blakish-overlay"></div>
                            </div>
                            <div class="best-course-text">
                                <div class="course-title mb20 headline relative-position">
                                    <h3><a href="{{$course->slug}}">{{$course->title}}</a></h3>
                                </div>
                                <div class="course-meta">
                                    @if (!empty($course->category))
                                    <span class="course-category"><a href="{{route('course', ['category'=>$course->category->id])}}">{{$course->category->category}}</a></span>
                                    @endif
                                    {{-- <span class="course-author"><a href="#">250 Students</a></span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /course -->
                @endforeach
            </div>
        </div>
        <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font" style="margin: 0 auto; margin-top: 65px;">
            <a href="{{route('course')}}">Lihat Semua <i class="fas fa-caret-right"></i></a>
        </div>
    </div>
</section>
@endif