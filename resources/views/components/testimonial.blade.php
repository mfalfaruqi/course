<section id="testimonial_2" class="testimonial_2_section">
    <div class="container">
        <div class="testimonial-slide">
            <div class="section-title-2 mb65 headline text-left">
                <h2>Students <span>Testimonial.</span></h2>
            </div>

            <div  id="testimonial-slide-item" class="testimonial-slide-area">
                <div class="student-qoute">
                    <p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience was good from start to finish, so we’ll be back in the future lorem ipsum diamet.”</p>
                    <div class="student-name-designation">
                        <span class="st-name bold-font">Robertho Garcia </span>
                        <span class="st-designation">Graphic Designer</span>
                    </div>
                </div>

                <div class="student-qoute">
                    <p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience was good from start to finish, so we’ll be back in the future lorem ipsum diamet.”</p>
                    <div class="student-name-designation">
                        <span class="st-name bold-font">Robertho Garcia </span>
                        <span class="st-designation">Graphic Designer</span>
                    </div>
                </div>

                <div class="student-qoute">
                    <p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience was good from start to finish, so we’ll be back in the future lorem ipsum diamet.”</p>
                    <div class="student-name-designation">
                        <span class="st-name bold-font">Robertho Garcia </span>
                        <span class="st-designation">Graphic Designer</span>
                    </div>
                </div>

                <div class="student-qoute">
                    <p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience was good from start to finish, so we’ll be back in the future lorem ipsum diamet.”</p>
                    <div class="student-name-designation">
                        <span class="st-name bold-font">Robertho Garcia </span>
                        <span class="st-designation">Graphic Designer</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>