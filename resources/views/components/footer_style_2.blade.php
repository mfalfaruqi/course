<div class="footer_2 backgroud-style">
    <div class="container">
        <div class="back-top text-center mb45">
            <a class="scrollup" href="#"><img src="{{url('/')}}/img/banner/bt.png" alt=""></a>
        </div>
        <div class="footer_2_logo text-center">
            <img src="{{url('/')}}/img/logo/logo.png" alt="">
        </div>

        <div class="footer_2_subs text-center">
            <p>We take our mission of increasing global access to quality education seriously. </p>
            <div class="subs-form relative-position">
                <form action="#" method="post">
                    <input class="course" name="course" type="email" placeholder="Email Address.">
                    <div class="nws-button text-center  gradient-bg text-uppercase">
                        <button type="submit" value="Submit">Subscribe now</button> 
                    </div>
                </form>
            </div>
        </div>
        <div class="copy-right-menu">
            <div class="row">
                <div class="col-md-5">
                    <div class="copy-right-text">
                        <p>© 2018 - www.GeniusCourse.com. All rights reserved</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-social  text-center ul-li">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="copy-right-menu-item float-right ul-li">
                        <ul>
                            <li><a href="#">License</a></li>
                            <li><a href="#">Privacy & Policy</a></li>
                            <li><a href="#">Term Of Service</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>