<section id="contact_secound" class="contact_secound_section backgroud-style">
    <div class="container">
        <div class="contact_secound_content">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-left-content">
                        <div class="section-title  mb45 headline text-left">
                            <span class="subtitle ml42  text-uppercase">CONTACT US</span>
                            <h2><span>Get in Touch</span></h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet  ipsum dolor sit amet, consectetuer adipiscing elit.
                            </p>
                        </div>

                        <div class="contact-address">
                            <div class="contact-address-details">
                                <div class="address-icon relative-position text-center float-left">
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                <div class="address-details ul-li-block">
                                    <ul>
                                        <li><span>Primary: </span>Last Vegas, 120 Graphic Street, US</li>
                                        <li><span>Second: </span>Califorinia, 88 Design Street, US</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="contact-address-details">
                                <div class="address-icon relative-position text-center float-left">
                                    <i class="fas fa-phone"></i>
                                </div>
                                <div class="address-details ul-li-block">
                                    <ul>
                                        <li><span>Primary: </span>(100) 3434 55666</li>
                                        <li><span>Second: </span>(20) 3434 9999</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="contact-address-details">
                                <div class="address-icon relative-position text-center float-left">
                                    <i class="fas fa-envelope"></i>
                                </div>
                                <div class="address-details ul-li-block">
                                    <ul>
                                        <li><span>Primary: </span>info@geniuscourse.com</li>
                                        <li><span>Second: </span>mail@genius.info</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="contact_secound_form">
                        <div class="section-title-2 mb65 headline text-left">
                            <h2>Send Us a message</h2>
                        </div>
                        <form class="contact_form" action="#" method="POST" enctype="multipart/form-data">
                            <div class="contact-info">
                                <input class="name" name="name" type="text" placeholder="Your Name.">
                            </div>
                            <div class="contact-info">
                                <input class="email" name="email" type="email" placeholder="Your Email">
                            </div>
                            <textarea  placeholder="Message."></textarea>
                            <div class="nws-button text-center  gradient-bg text-capitalize">
                                <button type="submit" value="Submit">SEND MESSAGE NOW <i class="fas fa-caret-right"></i></button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>