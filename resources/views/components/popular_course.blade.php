<section id="popular-course" class="popular-course-section">
    <div class="container">
        <div class="section-title mb20 headline text-left">
            <span class="subtitle text-uppercase">LEARN NEW SKILLS</span>
            <h2><span>Popular</span> Courses.</h2>
        </div>
        <div id="course-slide-item" class="course-slide">
            <div class="course-item-pic-text">
                <div class="course-pic relative-position mb25">
                    <img src="{{url('/')}}/img/course/c-1.jpg" alt="">
                    <div class="course-price text-center gradient-bg">
                        <span>$99.00</span>
                    </div>
                    <div class="course-details-btn">
                        <a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="course-item-text">
                    <div class="course-meta">
                        <span class="course-category bold-font"><a href="#">Web Design</a></span>
                        <span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
                        <div class="course-rate ul-li">
                            <ul>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="course-title mt10 headline pb45 relative-position">
                        <h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
                    </div>
                    <div class="course-viewer ul-li">
                        <ul>
                            <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
                            <li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
                            <li><a href="">125k Unrolled</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <div class="course-item-pic-text">
                <div class="course-pic relative-position mb25">
                    <img src="{{url('/')}}/img/course/c-2.jpg" alt="">
                    <div class="course-price text-center gradient-bg">
                        <span>$99.00</span>
                    </div>
                    <div class="course-details-btn">
                        <a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="course-item-text">
                    <div class="course-meta">
                        <span class="course-category bold-font"><a href="#">Mobile Apps</a></span>
                        <span class="course-author bold-font"><a href="#">Fernando Torres</a></span>
                        <div class="course-rate ul-li">
                            <ul>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="course-title mt10 headline pb45 relative-position">
                        <h3><a href="#">Introduction to Mobile Application Development.</a></h3>
                    </div>
                    <div class="course-viewer ul-li">
                        <ul>
                            <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
                            <li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
                            <li><a href="">125k Unrolled</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <div class="course-item-pic-text">
                <div class="course-pic relative-position mb25">
                    <img src="{{url('/')}}/img/course/c-3.jpg" alt="">
                    <div class="course-price text-center gradient-bg">
                        <span>$99.00</span>
                    </div>
                    <div class="course-details-btn">
                        <a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="course-item-text">
                    <div class="course-meta">
                        <span class="course-category bold-font"><a href="#">Motion Graphic </a></span>
                        <span class="course-author bold-font"><a href="#">enny Garcias</a></span>
                        <div class="course-rate ul-li">
                            <ul>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="course-title mt10 headline pb45 relative-position">
                        <h3><a href="#">Learning IOS Apps Programming & Development.</a></h3>
                    </div>
                    <div class="course-viewer ul-li">
                        <ul>
                            <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
                            <li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
                            <li><a href="">125k Unrolled</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <div class="course-item-pic-text">
                <div class="course-pic relative-position mb25">
                    <img src="{{url('/')}}/img/course/c-2.jpg" alt="">
                    <div class="course-price text-center gradient-bg">
                        <span>$99.00</span>
                    </div>
                    <div class="course-details-btn">
                        <a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="course-item-text">
                    <div class="course-meta">
                        <span class="course-category bold-font"><a href="#">Web Design</a></span>
                        <span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
                        <div class="course-rate ul-li">
                            <ul>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="course-title mt10 headline pb45 relative-position">
                        <h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
                    </div>
                    <div class="course-viewer ul-li">
                        <ul>
                            <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
                            <li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
                            <li><a href="">125k Unrolled</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <div class="course-item-pic-text">
                <div class="course-pic relative-position mb25">
                    <img src="{{url('/')}}/img/course/c-3.jpg" alt="">
                    <div class="course-price text-center gradient-bg">
                        <span>$99.00</span>
                    </div>
                    <div class="course-details-btn">
                        <a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="course-item-text">
                    <div class="course-meta">
                        <span class="course-category bold-font"><a href="#">Web Design</a></span>
                        <span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
                        <div class="course-rate ul-li">
                            <ul>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="course-title mt10 headline pb45 relative-position">
                        <h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
                    </div>
                    <div class="course-viewer ul-li">
                        <ul>
                            <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
                            <li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
                            <li><a href="">125k Unrolled</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <div class="course-item-pic-text">
                <div class="course-pic relative-position mb25">
                    <img src="{{url('/')}}/img/course/c-1.jpg" alt="">
                    <div class="course-price text-center gradient-bg">
                        <span>$99.00</span>
                    </div>
                    <div class="course-details-btn">
                        <a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="course-item-text">
                    <div class="course-meta">
                        <span class="course-category bold-font"><a href="#">Web Design</a></span>
                        <span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
                        <div class="course-rate ul-li">
                            <ul>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="course-title mt10 headline pb45 relative-position">
                        <h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
                    </div>
                    <div class="course-viewer ul-li">
                        <ul>
                            <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
                            <li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
                            <li><a href="">125k Unrolled</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /item -->
        </div>
    </div>
</section>