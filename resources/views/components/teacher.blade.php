@if (!empty($coaches))
<section id="genius-teacher-2" class="genius-teacher-section-2">
    <div class="container">
        <div class="section-title mb35 headline text-left">
            <span class="subtitle ml42  text-uppercase">Educators</span>
            <h2>WGrow.id <span>Educators.</span></h2>
        </div>
        <div class="teacher-third-slide">
            @foreach ($coaches as $coach)
            <div class="teacher-double">
                <div class="teacher-img-content relative-position">
                    @if (!empty($coach->picture_url))
                        <a href="tes"><img src="{{Storage::url($coach->picture_url)}}" alt="{{$coach->name}}"></a>
                    @else
                        <img src="{{url('/')}}/img/teacher/default.jpg" alt="{{$coach->name}}">
                    @endif
                    <div class="teacher-cntent">
                        <div class="teacher-social-name ul-li-block">
                            <ul>
                                @if (!empty($coach->facebook)) <li><a href="{{$coach->facebook}}"><i class="fab fa-facebook-f"></i></a></li>@endif
                                @if (!empty($coach->twitter)) <li><a href="{{$coach->twitter}}"><i class="fab fa-twitter"></i></a></li>@endif
                                @if (!empty($coach->instagram)) <li><a href="{{$coach->instagram}}"><i class="fab fa-instagram"></i></a></li>@endif
                            </ul>
                            <div class="teacher-name">
                                <a href="{{$coach->slug}}"><span>{{$coach->name}}</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif