<section id="contact-page" class="contact-page-section">
    <div class="container">
        <div class="section-title mb45 headline text-center">
            <span class="subtitle text-uppercase">SEND US A MESSAGE</span>
            <h2>Keep<span> In Touch.</span></h2>
        </div>
        <div class="social-contact">
            <div class="category-icon-title text-center">
                <div class="category-icon">
                    <i class="text-gradiant fab fa-facebook-f"></i>
                </div>
                <div class="category-title">
                    <h4>Facebook</h4>
                </div>
            </div>
            <div class="category-icon-title text-center">
                <div class="category-icon">
                    <i class="text-gradiant fab fa-instagram"></i>
                </div>
                <div class="category-title">
                    <h4>Instagram</h4>
                </div>
            </div>
            <div class="category-icon-title text-center">
                <div class="category-icon">
                    <i class="text-gradiant fab fa-whatsapp"></i>
                </div>
                <div class="category-title">
                    <h4>Whatsapp</h4>
                </div>
            </div>
        </div>
    </div>
</section>