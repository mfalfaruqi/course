<style>
</style>
<section id="slide" class="slider-section">
        <div id="slider-item" class="slider-item-details">
        @foreach ($banners as $banner)
            <div class="slider-area slider-bg-5 relative-position" style="background-image: url({{Storage::url($banner->desktop_banner)}});">
                <div class="slider-text">
                        <div class="section-title mb20 headline text-center ">
                            <div class="layer-1-1">
                                <span class="subtitle ml42 text-uppercase">Educators AGENCY PROJECT</span>
                            </div>
                            <div class="layer-1-3">
                                <h2><span>WGrow.id</span> <br> Your Growing <span>Enabler</span></h2>
                            </div>
                        </div>
                        <div class="layer-1-4">
                            <div class="about-btn text-center">
                                <div class="genius-btn text-center text-uppercase ul-li-block bold-font">
                                    <a href="{{route('coach')}}">See Educators <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                </div>    
                {{-- <a href="{{$banner->url}}"><img src="{{Storage::url($banner->desktop_banner)}}" class="img-center"></a> --}}
            </div>    
        @endforeach
    </div>
</section>