@if (!empty($blogs))
<section id="best-course" class="best-course-section" style="padding: 0px 0px 50px 0px;">
    <div class="container">
        <div class="section-title mb35 headline text-left">
            <span class="subtitle ml42 text-uppercase">Artikel</span>
            <h2>Artikel<span> Terbaru.</span></h2>
        </div>
        <div class="best-course-area mb45">
            <div class="row">
                @foreach ($blogs as $blog)
                <div class="col-md-4">
                    <div class="best-course-pic-text relative-position">
                        <a href="{{$blog->slug}}">
                            <div class="best-course-pic relative-position">
                                <img src="{{Storage::url($blog->banner_url)}}" alt="">
                                <div class="course-details-btn">
                                    <a href="{{$blog->slug}}">BACA LEBIH LANJUT <i class="fas fa-arrow-right"></i></a>
                                </div>
                                <div class="blakish-overlay"></div>
                            </div>
                        </a>
                        <div class="best-course-text">
                            <div class="course-title mb20 headline relative-position">
                                <h3><a href="{{$blog->slug}}">{{$blog->title}}</a></h3>
                            </div>
                            <div class="course-title mb20 headline relative-position">
                                <p>{{str_limit($blog->highlight, 100)}}</p>    
                            </div>
                            <div class="course-meta">
                                <span class="course-category"><a href="{{route('blog', ['category' => $blog->category->id])}}">{{$blog->category->category}}</a></span>
                                <span class="course-author">{{\Carbon\Carbon::parse($blog->created_at)->format('Y-m-d')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /blog -->
                @endforeach
            </div>
        </div>

        <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font" style="margin: 0 auto; margin-top: 65px;">
            <a href="{{route('coach')}}">Lihat Semua <i class="fas fa-caret-right"></i></a>
        </div>
    </div>
</section>
@endif