<section id="contact-area" class="contact-area-section backgroud-style" style="background-image: url(../img/banner/contact-b-red.jpg);">
    <div class="container">
        <div class="contact-area-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-left-content">
                        <div class="section-title  mb45 headline text-left">
                            <span class="subtitle ml42  text-uppercase">HUBUNGI KAMI</span>
                            <h2><span>WGrow.id</span></h2>
                            <p>
                                
                            </p>
                        </div>

                        <div class="contact-address">
                            <div class="contact-address-details">
                                <div class="address-icon relative-position text-center float-left">
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                <div class="address-details ul-li-block">
                                    <ul>
                                        <li><span>Lokasi: </span>Hotel The Radiant Center</li>
                                        <li><span>Alamat: </span>Jl. H. Abdul Gani, No. 17 A, Cempaka Putih, Ciputat Timur, Tangerang Selatan </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="contact-address-details">
                                <div class="address-icon relative-position text-center float-left">
                                    <i class="fas fa-phone"></i>
                                </div>
                                <div class="address-details ul-li-block">
                                    <ul>
                                        <li><span>Telepon: </span>PT. SIMS (Sukses Insan Mulia Sejahtera)</li>
                                        <li><span>Nomor: </span>081212447887 / 0217495434</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="contact-address-details">
                                <div class="address-icon relative-position text-center float-left">
                                    <i class="fas fa-envelope"></i>
                                </div>
                                <div class="address-details ul-li-block">
                                    <ul>
                                        <li><span>Email: </span>info@wgrow.id</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="col-md-6">
                    <div class="embed-responsive embed-responsive-1by1">
                            {{-- <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15862.986240004513!2d106.7573882!3d-6.2969985!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4f855a931f459725!2sThe+Radiant+Center!5e0!3m2!1sen!2sid!4v1554300255099!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
                            <!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/86c8eeebaa6b5e4b9e48ce5ec72899b6.html" scrolling="no" allowtransparency="true" class="lightwidget-widget embed-responsive-item" style="width:100%;border:0;overflow:hidden;"></iframe>
                    </div>
                </div>
                

            </div>
            

        </div>
    </div>
</section>
<script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>