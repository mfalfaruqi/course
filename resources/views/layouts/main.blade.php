<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<title>{{ config('app.name') }} @yield('title') </title>
	<link rel="icon" href="{{url('/')}}/img/logo/new-logo-wg-icon.png">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="{{url('/')}}/css/owl.carousel.css">
	<link rel="stylesheet" href="{{url('/')}}/css/fontawesome-all.css">
	<link rel="stylesheet" href="{{url('/')}}/css/flaticon/flaticon.css">
	
	<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/meanmenu.css">
	<link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{url('/')}}/css/video.min.css">
	<link rel="stylesheet" href="{{url('/')}}/css/animate.min.css">
	<link rel="stylesheet" href="{{url('/')}}/css/lightbox.css">
	<link rel="stylesheet" href="{{url('/')}}/css/progess.css">
	<link rel="stylesheet" href="{{ mix('/css/style.css') }}">
	<link rel="stylesheet" href="{{url('/')}}/css/responsive.css">
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<link rel="stylesheet" href="{{ mix('/css/custom.css') }}">
	@yield('additional_css')

	<!-- For Js Library -->
    <script src="{{url('/')}}/js/jquery-2.1.4.min.js"></script>
	<script src="{{url('/')}}/js/popper.min.js"></script>
	<script src="{{url('/')}}/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/js/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/js/jarallax.js"></script>
    <script src="{{url('/')}}/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('/')}}/js/lightbox.js"></script>
    <script src="{{url('/')}}/js/jquery.meanmenu.js"></script>
    <script src="{{url('/')}}/js/scrollreveal.min.js"></script>
    <script src="{{url('/')}}/js/jquery.counterup.min.js"></script>
    <script src="{{url('/')}}/js/waypoints.min.js"></script>
    <script src="{{url('/')}}/js/jquery-ui.js"></script>
    <script src="{{url('/')}}/js/gmap3.min.js"></script>
	<script src="{{url('/')}}/js/switch.js"></script>
	<script src="{{url('/')}}/js/instafeed.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>
	<script src="{{url('/')}}/js/script.js"></script>
	<script src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	<script src="{{ mix('/js/custom.js') }}"></script>
	@yield('additional_js')

</head>

<body>

    <!-- Start of Header section
	============================================= -->
	@include('components.header')
	<!-- Start of Header section
    ============================================= -->
    
    @yield('body')

    <!-- Start Of footer section
	============================================= -->
	@include('components.footer')
	<!-- End Of footer section
	============================================= -->
</body>
</html>