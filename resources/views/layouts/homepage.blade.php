@extends('layouts.main')
@section('body')
	<!-- Start of slider section
	============================================= -->
	@include('components.home_slider')
	<!-- End of slider section
	============================================= -->

	<!-- Start of sponsor section
	============================================= -->
	{{-- @include('components.sponsor') --}}
	<!-- End of sponsor section
	============================================= -->

	<!-- Start popular course
	============================================= -->
	{{-- @include('components.popular_course') --}}
	<!-- End popular course
	============================================= -->


	<!-- Start of about us section
	============================================= -->
	@include('components.about_us_sort')
	<!-- End of about us section
	============================================= -->


	<!-- Start of Search Courses
	============================================= -->
	{{-- @include('components.search_course') --}}
	<!-- End of Search Courses
	============================================= -->


	<!-- Start latest section
	============================================= -->
	{{-- @include('components.news') --}}
	<!-- End latest section
	============================================= -->


	<!-- Start of best product section
	============================================= -->
	{{-- @include('components.product') --}}
	<!-- End  of best product section
	============================================= -->


	<!-- Start of best course
	============================================= -->
	@include('components.course')
	<!-- End of best course
	============================================= -->


	<!-- Start FAQ section
	============================================= -->
	{{-- @include('components.faq') --}}
	<!-- End FAQ section
	============================================= -->


	<!-- Start Course category
	============================================= -->
	{{-- @include('components.category') --}}
	<!-- End Course category
	============================================= -->


	<!-- Start secound testimonial section
	============================================= -->
	{{-- @include('components.testimonial') --}}
	<!-- End secound testimonial section
	============================================= -->


	<!-- Start secound teacher section
	============================================= -->
	@include('components.teacher_circle')
	<!-- End teacher section
	============================================= -->

	<!-- Start recent blog
	============================================= -->
	@include('components.blog')
	<!-- End recent blog
	============================================= -->

	<!-- Start contact with map
	============================================= -->
	@include('components.contact_with_map')
	<!-- End contact with map
	============================================= -->
@endsection





		