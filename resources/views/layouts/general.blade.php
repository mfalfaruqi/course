
@extends('layouts.main')
@section('body')
	<!-- Start of breadcrumb section
		============================================= -->
		<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style" style="background-image:url('/img/banner/brt-1.jpg')">
			<div class="blakish-overlay"></div>
			<div class="container">
				<div class="page-breadcrumb-content text-center">
					<div class="page-breadcrumb-title">
						<h2 class="breadcrumb-head black bold">@yield('page-title')</h2>
					</div>
					<div class="page-breadcrumb-item ul-li">
						<ul class="breadcrumb text-uppercase black">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">@yield('page_name')</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	<!-- End of breadcrumb section
		============================================= -->

	@if ($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong>Terjadi masalah!</strong> {{ $error }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endforeach
	@endif

	@if (session('status'))
		<div class="alert alert-success alert-dismissible fade show" role="alert">
				<strong>Sukses!</strong> {{ session('status') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif

	@yield('content')
@endsection
    