@extends('layouts.general')
@section('page_name')
    Kategori
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Kategori') }}</div>
                    <div class="card-body">
                        <form method="POST" action="@if (!empty($category->id)) {{ route('admin.categories.update', $category->id)}} @else {{ route('admin.categories.add')}} @endif" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($category->id))
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{ __('ID') }}</label>

                                    <div class="col-md-6">
                                        <input class="form-control" value="{{ $category->id }}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Kategori') }}</label>

                                <div class="col-md-6">
                                    <input class="form-control" name="category" value="@if (!empty($category->category)){{ $category->category }} @endif" required>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a class="btn btn-link" href="{{ route('admin.categories') }}">
                                        Batal
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#description').summernote();
    });
    
</script>
@endsection
