@extends('layouts.general')
@section('page_name')
    Pesanan
@endsection

@section('content')
    <!-- Start of about us content
    ============================================= -->
    <section id="about-page" class="about-page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2>Daftar Pesanan</h2><br>
                    <table class="table table-striped table-bordered" id="orders-table">
                        <thead>
                            <tr>
                                <th>Nomor Pesanan</th>
                                <th>Workshop</th>
                                <th>Tanggal Acara</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <script>
                $(function() {
                    $('#orders-table').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route('admin.orders.gets') !!}',
                        columns: [
                            { data: 'order_number', name: 'order_number' },
                            { data: 'course_title', name: 'course_title' },
                            { data: 'course_start', name: 'course_start' },
                            { data: 'status', name: 'status' },
                            {
                                data: 'detail_url',
                                name: 'detail_url',
                                searchable: false,
                                render: function ( data, type, row, meta ) {
                                return "<a href='"+data+"'><button class='btn btn-info btn-sm'>Detail/Konfirmasi</button></a>";
                                },
                            }
                        ],
                        order: [[ 0, "desc" ]]
                    });
                });
                </script>

                @include('admin.sidebar')
            </div>
        </div>
    </section>
    <!-- End of about us content
        ============================================= -->
@endsection