@extends('layouts.general')
@section('page_name')
    Banner
@endsection

@section('content')
    <!-- Start of about us content
    ============================================= -->
    <section id="about-page" class="about-page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2>Daftar Banner</h2>
                </div>
                <div class="col-md-9">
                    <h2><a href='{{route('admin.banners.add_index')}}'><button class='btn btn-primary btn-sm'>Tambah</button></a><h2>
                </div>
                <div class="col-md-9">
                    <table class="table table-striped table-bordered" id="banners-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Desktop</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <script>
                $(function() {
                    $('#banners-table').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route('admin.banners.gets') !!}',
                        columns: [
                            { data: 'id', name: 'id' },
                            { data: 'name', name: 'name' },
                            { 
                                data: 'desktop_banner_url',
                                name: 'desktop_banner_url',
                                searchable: false,
                                sortable: false,
                                render: function ( data, type, row, meta ) {
                                    return "<img src='"+data+"'>";
                                },
                            },
                            {
                                data: 'detail_url',
                                name: 'detail_url',
                                searchable: false,
                                sortable: false,
                                render: function ( data, type, row, meta ) {
                                    return "<a href='"+data+"'><button class='btn btn-info btn-sm'>Ubah</button></a> <button class='btn btn-danger btn-sm btn-delete' data-url='"+data+"' data-name='"+row.name+"'>Hapus</button>";
                                },
                            }
                        ],
                        order: [[ 0, "desc" ]]
                    });
                });
                </script>

                @include('admin.sidebar')
            </div>
        </div>
    </section>
    <!-- End of about us content
        ============================================= -->
    
    <!-- Modal Delete Confirmation -->
    <div class="modal fade" id="delete-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="delete-confirmation-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Konfirmasi Hapus Educator</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda yakin ingin menghapus <span id="coach-name"></span>?</p> <br>
                </div>
                <div class="modal-footer">
                    <form method="POST" id="delete-form" action="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')
                    
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary" value="Delete">Ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).on('click', '.btn-delete', function(){
            $('#delete-confirmation-modal').modal('show');
            $('#delete-form').attr('action', $(this).data('url'));
            $('#coach-name').text($(this).data('name'));
        });
    </script>
@endsection