@extends('layouts.general')
@section('page_name')
    Pesanan
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Pesanan') }}</div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Nomor Pesanan') }}</label>

                            <div class="col-md-6">
                                <input class="form-control disabledInput" value="{{ $order->order_number }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Status Pesanan') }}</label>

                            <div class="col-md-6">
                                <input class="form-control disabledInput" value="{{ $order->orderStatus->order_status }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Pembayaran') }}</label>

                            <div class="col-md-6">
                                <input class="form-control disabledInput" value="Rp{{number_format($order->invoice_price, 2, ',', '.')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Bukti Pembayaran') }}</label>
                            <div class="col-md-6">
                                @if (!empty($order->acquittance_url))
                                    <a href="{{ Storage::url($order->acquittance_url)}}" id="acquittance-url-value" target="_blank"><img src="{{ Storage::url($order->acquittance_url)}}" style="max-width:100%; height:auto;"></a>
                                @else
                                    <input class="form-control disabledInput" value="User belum upload bukti pembayaran.">
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                <div class="genius-btn gradient-bg text-center text-uppercase float-left bold-font">
                                    <a href="#payment-confirmation-modal" data-toggle="modal" data-target="#payment-confirmation-modal">Konfirmasi Pelunasan</a>
                                </div>
                                <a class="btn btn-link" href="{{ route('admin.orders') }}">
                                    Batal
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Detail Pemesan') }}</div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->user->name }}</span>
                            </div>
                        </div>    
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->user->email }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Telepon') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >@if (empty($order->user->phone)) - @endif{{ $order->user->phone }} </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >@if (empty($order->user->address)) - @endif{{ $order->user->address }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Detail Pesanan') }}</div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Workshop') }}</label>

                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->course->title }}</span>
                            </div>
                        </div>    
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Mulai') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ Carbon\Carbon::parse($order->course->start_date . $order->course->start_time) }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Selesai') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ Carbon\Carbon::parse($order->course->end_date . $order->course->end_time) }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Lokasi') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->course->venue }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Alamat Lokasi') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->course->venue_address }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>
                            <div class="col-md-6">
                                <a class="btn btn-sm btn-light" href="{{ route('course_detail', [str_slug($order->course->title), $order->course->id]) }}">Klik di sini</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Confirmation -->
<div class="modal fade" id="payment-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="payment-confirmation-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Pelunasan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin untuk mengubah status Pesanan {{$order->order_number}} menjadi lunas?</p> <br>
                <p>Nama Pemesan : {{$order->user->name}}</p>
            </div>
            <div class="modal-footer">
                <form method="POST" action="{{ route('admin.orders.update', $order->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="payment_confirmation" value="true">
                
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-primary">Ya</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
