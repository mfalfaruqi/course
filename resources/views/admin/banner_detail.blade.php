@extends('layouts.general')
@section('page_name')
    Banner
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Banner') }}</div>
                    <div class="card-body">
                        <form method="POST" action="@if (!empty($banner->id)) {{ route('admin.banners.update', $banner->id)}} @else {{ route('admin.banners.add')}} @endif" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($banner->id))
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">{{ __('ID') }}</label>

                                    <div class="col-md-6">
                                        <input class="form-control" value="{{ $banner->id }}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                                <div class="col-md-6">
                                    <input class="form-control" name="name" value="@if (!empty($banner->name)){{ $banner->name }} @endif" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Desktop Banner (1980x980)') }}</label>
                                <div class="col-md-6">
                                    @if (!empty($banner->desktop_banner))
                                        <a href="{{ Storage::url($banner->desktop_banner)}}" id="desktop-banner-value" target="_blank"><img src="{{ Storage::url($banner->desktop_banner)}}" style="max-width:100%; height:auto;"></a>
                                        <input id="desktop-banner-input" type="file" name="desktop_banner" class="form-control" style="display: none;" disabled accept="image/*">
                                        <div class="input-group-append">
                                            <button id="reupload-button-desktop" class="btn btn-outline-secondary btn-sm" type="button">Upload Ulang</button>
                                        </div>

                                        <script>
                                            $('#reupload-button-desktop').click(function(){
                                                $('#desktop-banner-value').hide();
                                                $('#desktop-banner-input').show();
                                                $('#desktop-banner-input').removeAttr("disabled")
                                                $('#reupload-button').hide();
                                            });
                                        </script>
                                    @else
                                        <input type="file" name="desktop_banner" class="form-control" accept="image/*">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Mobile Banner (980x980)') }}</label>
                                <div class="col-md-6">
                                    @if (!empty($banner->mobile_banner))
                                        <a href="{{ Storage::url($banner->mobile_banner)}}" id="mobile-banner-value" target="_blank"><img src="{{ Storage::url($banner->mobile_banner)}}" style="max-width:100%; height:auto;"></a>
                                        <input id="mobile-banner-input" type="file" name="mobile_banner" class="form-control" style="display: none;" disabled accept="image/*">
                                        <div class="input-group-append">
                                            <button id="reupload-button-mobile" class="btn btn-outline-secondary btn-sm" type="button">Upload Ulang</button>
                                        </div>

                                        <script>
                                            $('#reupload-button-mobile').click(function(){
                                                $('#mobile-banner-value').hide();
                                                $('#mobile-banner-input').show();
                                                $('#mobile-banner-input').removeAttr("disabled")
                                                $('#reupload-button').hide();
                                            });
                                        </script>
                                    @else
                                        <input type="file" name="mobile_banner" class="form-control" accept="image/*">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('URL') }}</label>

                                <div class="col-md-6">
                                    <input class="form-control" name="url" value="@if (!empty($banner->url)){{ $banner->url }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Posisi') }}</label>

                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="position" value="<?php if (!empty($banner->position)) {echo $banner->position;} else {echo 0;} ?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Status Aktif') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="fg_active">
                                        <option value="0">Tidak Aktif</option>
                                        <option value="1" @if (!empty($banner->fg_active) && $banner->fg_active == 1) selected @endif>Aktif</option>    
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a class="btn btn-link" href="{{ route('admin.banners') }}">
                                        Batal
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#description').summernote();
    });
    
</script>
@endsection
