@extends('layouts.general')
@section('page_name')
    Artikel
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Artikel') }}</div>
                    <div class="card-body">
                        <form method="POST" action="@if (!empty($blog->id)) {{ route('admin.blogs.update', $blog->id)}} @else {{ route('admin.blogs.add')}} @endif" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($blog->id))
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">{{ __('ID') }}</label>

                                    <div class="col-md-10">
                                        <input class="form-control" value="{{ $blog->id }}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Judul') }}</label>

                                <div class="col-md-10">
                                    <input class="form-control" name="title" value="@if (!empty($blog->title)){{ $blog->title }} @endif" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Kategori') }}</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="blog_category_id" required>
                                        <option value="">- Pilih -</option>
                                        @foreach ($blogCategories as $blogCategory)
                                            <option value="{{$blogCategory->id}}" @if (!empty($blog->blog_category_id) && $blogCategory->id == $blog->blog_category_id) selected @endif>{{$blogCategory->category}}</option>    
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Foto Banner') }}</label>
                                <div class="col-md-10">
                                    @if (!empty($blog->banner_url))
                                        <a href="{{ Storage::url($blog->banner_url)}}" id="picture-url-value" target="_blank"><img src="{{ Storage::url($blog->banner_url)}}" style="max-width:100%; height:auto;"></a>
                                        <input id="picture-url-input" type="file" name="banner_url" class="form-control" style="display: none;" disabled accept="image/*">
                                        <div class="input-group-append">
                                            <button id="reupload-button" class="btn btn-outline-secondary btn-sm" type="button">Upload Ulang</button>
                                        </div>
                                        <script>
                                            $('#reupload-button').click(function(){
                                                $('#picture-url-value').hide();
                                                $('#picture-url-input').show();
                                                $('#picture-url-input').removeAttr("disabled")
                                                $('#reupload-button').hide();
                                            });
                                        </script>
                                    @else
                                        <input type="file" name="banner_url" class="form-control" accept="image/*">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Highlight') }}</label>
                                <div class="col-md-10">
                                        <textarea id="highlight" name="highlight">@if (!empty($blog->highlight)){{ $blog->highlight }} @endif</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Body') }}</label>
                                <div class="col-md-10">
                                        <textarea id="body" name="body">@if (!empty($blog->body)){{ $blog->body }} @endif</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Educator') }}</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="coach_id" required>
                                        <option value="">- Pilih -</option>
                                        @foreach ($coaches as $coach)
                                            <option value="{{$coach->id}}" @if (!empty($blog->coach_id) && $coach->id == $blog->coach_id) selected @endif>{{$coach->name}}</option>    
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a class="btn btn-link" href="{{ route('admin.blogs') }}">
                                        Batal
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#body').summernote();
        $('select').selectpicker();
    });
    
</script>
@endsection
