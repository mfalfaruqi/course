<div class="col-md-3">
    <div class="side-bar-widget first-widget">
        <h2 class="widget-title text-capitalize"><span>Menu</span></h2>
        <div class = "btn-group-vertical col-sm-8">
            <button class="btn @if (\Route::current()->getName() == 'admin.orders') btn-primary @else btn-light @endif" onclick="location.href='{{route('admin.orders')}}';">Pesanan</button>
            <button class="btn @if (\Route::current()->getName() == 'admin.courses') btn-primary @else btn-light @endif" onclick="location.href='{{route('admin.courses')}}';">Workshop</button>
            <button class="btn @if (\Route::current()->getName() == 'admin.coaches') btn-primary @else btn-light @endif" onclick="location.href='{{route('admin.coaches')}}';">Educator</button>
            <button class="btn @if (\Route::current()->getName() == 'admin.categories') btn-primary @else btn-light @endif" onclick="location.href='{{route('admin.categories')}}';">Kategori</button>
            <button class="btn @if (\Route::current()->getName() == 'admin.banners') btn-primary @else btn-light @endif" onclick="location.href='{{route('admin.banners')}}';">Banner</button>
            <button class="btn @if (\Route::current()->getName() == 'admin.blogs') btn-primary @else btn-light @endif" onclick="location.href='{{route('admin.blogs')}}';">Artikel</button>
        </div>
    </div>
</div>