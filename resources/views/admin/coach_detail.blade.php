@extends('layouts.general')
@section('page_name')
    Educator
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Educator') }}</div>
                    <div class="card-body">
                        <form method="POST" action="@if (!empty($coach->id)) {{ route('admin.coaches.update', $coach->id)}} @else {{ route('admin.coaches.add')}} @endif" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($coach->id))
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">{{ __('ID') }}</label>

                                    <div class="col-md-10">
                                        <input class="form-control" value="{{ $coach->id }}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Nama') }}</label>

                                <div class="col-md-10">
                                    <input class="form-control" name="name" value="@if (!empty($coach->name)){{ $coach->name }} @endif" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Profil Singkat') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="highlight" value="@if (!empty($coach->highlight)){{ $coach->highlight }} @endif">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Deskripsi') }}</label>
                                <div class="col-md-10">
                                        <textarea id="description" name="description">@if (!empty($coach->description)){{ $coach->description }} @endif</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Foto Profil') }}</label>
                                <div class="col-md-10">
                                    @if (!empty($coach->picture_url))
                                        <a href="{{ Storage::url($coach->picture_url)}}" id="picture-url-value" target="_blank"><img src="{{ Storage::url($coach->picture_url)}}" style="max-width:100%; height:auto;"></a>
                                        <input id="picture-url-input" type="file" name="picture_url" class="form-control" style="display: none;" disabled accept="image/*">
                                        <div class="input-group-append">
                                            <button id="reupload-button" class="btn btn-outline-secondary btn-sm" type="button">Upload Ulang</button>
                                        </div>

                                        <script>
                                            $('#reupload-button').click(function(){
                                                $('#picture-url-value').hide();
                                                $('#picture-url-input').show();
                                                $('#picture-url-input').removeAttr("disabled")
                                                $('#reupload-button').hide();
                                            });
                                        </script>
                                    @else
                                        <input type="file" name="picture_url" class="form-control" accept="image/*">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Alamat') }}</label>
                                <div class="col-md-10">
                                        <textarea name="address">@if (!empty($coach->address)){{ $coach->address }} @endif</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Telepon') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="phone" value="@if (!empty($coach->phone)){{ $coach->phone }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="email" value="@if (!empty($coach->email)){{ $coach->email }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('URL Facebook') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="facebook" value="@if (!empty($coach->facebook)){{ $coach->facebook }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('URL Twitter') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="twitter" value="@if (!empty($coach->twitter)){{ $coach->twitter }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('URL Instagram') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="instagram" value="@if (!empty($coach->instagram)){{ $coach->instagram }} @endif">
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a class="btn btn-link" href="{{ route('admin.coaches') }}">
                                        Batal
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#description').summernote();
    });
    
</script>
@endsection
