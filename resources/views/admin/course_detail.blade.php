@extends('layouts.general')
@section('page_name')
    Workshop
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Workshop') }}</div>
                    <div class="card-body">
                        <form method="POST" action="@if (!empty($course->id)) {{ route('admin.courses.update', $course->id)}} @else {{ route('admin.courses.add')}} @endif" enctype="multipart/form-data">
                            @csrf
                            @if (!empty($course->id))
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">{{ __('ID') }}</label>

                                    <div class="col-md-10">
                                        <input class="form-control" value="{{ $course->id }}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Judul') }}</label>

                                <div class="col-md-10">
                                    <input class="form-control" name="title" value="@if (!empty($course->title)){{ $course->title }} @endif" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Kategori') }}</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="category_id">
                                        <option value="">- Pilih -</option>
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}" @if (!empty($course->category_id) && $category->id == $course->category_id) selected @endif>{{$category->category}}</option>    
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Type') }}</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="course_type_id">
                                        @foreach ($courseTypes as $courseType)
                                            <option value="{{$courseType->id}}" @if (!empty($course->course_type_id) && $courseType->id == $course->course_type_id) selected @endif>{{$courseType->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Deskripsi') }}</label>
                                <div class="col-md-10">
                                        <textarea id="description" name="description">@if (!empty($course->description)){{ $course->description }} @endif</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Foto Banner') }}</label>
                                <div class="col-md-10">
                                    @if (!empty($course->banner_url))
                                        <a href="{{ Storage::url($course->banner_url)}}" id="picture-url-value" target="_blank"><img src="{{ Storage::url($course->banner_url)}}" style="max-width:100%; height:auto;"></a>
                                        <input id="picture-url-input" type="file" name="banner_url" class="form-control" style="display: none;" disabled accept="image/*">
                                        <div class="input-group-append">
                                            <button id="reupload-button" class="btn btn-outline-secondary btn-sm" type="button">Upload Ulang</button>
                                        </div>
                                        <script>
                                            $('#reupload-button').click(function(){
                                                $('#picture-url-value').hide();
                                                $('#picture-url-input').show();
                                                $('#picture-url-input').removeAttr("disabled")
                                                $('#reupload-button').hide();
                                            });
                                        </script>
                                    @else
                                        <input type="file" name="banner_url" class="form-control" accept="image/*">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Tanggal Mulai (YYYY-MM-DD)') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="start_date" value="@if (!empty($course->start_date)){{ $course->start_date }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Waktu Mulai (HH:MM:SS)') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="start_time" value="@if (!empty($course->start_time)){{ $course->start_time }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Tanggal Selesai (YYYY-MM-DD)') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="end_date" value="@if (!empty($course->end_date)){{ $course->end_date }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Waktu Selesai (HH:MM:SS)') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="end_time" value="@if (!empty($course->end_time)){{ $course->end_time }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Lokasi') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="venue" value="@if (!empty($course->venue)){{ $course->venue }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Alamat Lokasi') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="venue_address" value="@if (!empty($course->venue_address)){{ $course->venue_address }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Biaya Pendaftaran') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="fee" value="@if (!empty($course->fee)){{ $course->fee }} @endif">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">{{ __('Educator') }}</label>
                                <div class="col-md-10">
                                    <select class="selectpicker" multiple data-live-search="true" name="coaches[]">
                                        @foreach ($coaches as $coach)
                                            <option value="{{$coach->id}}" @if (!empty($coachIDs) && in_array($coach->id, $coachIDs)) selected @endif>{{$coach->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a class="btn btn-link" href="{{ route('admin.courses') }}">
                                        Batal
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#description').summernote();
        $('select').selectpicker();
    });
    
</script>
@endsection
