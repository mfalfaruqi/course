@extends('layouts.general')
@section('page_name')
    Terima Kasih
@endsection

@section('title')
    Terima Kasih
@endsection

@section('content')
<section id="checkout" class="checkout-section">
	<div class="container">
		<div class="section-title mb45 headline text-center">
			<span class="subtitle text-uppercase">Terima Kasih</span>
			<h2>Pemesanan Anda <span>Berhasil.</span></h2>
		</div>
		<div class="section-title mb45 headline text-center">
			<p>Silahkan lakukan pembayaran untuk menyelesaikan pemesanan ini.</p>
		</div>
	</div>

	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-md-6 col-md-offset-">  
			<form>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Nomor Pesanan</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext" value="{{ $order->order_number }}">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Pemesan</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext" value="{{ $order->user->name }}">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Workshop</label>
					<div class="col-sm-8">
						<a href="{{$order->course->slug}}"><input type="text" readonly class="form-control-plaintext" value="{{ $order->course->title }}"></a>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
</section>
@endsection