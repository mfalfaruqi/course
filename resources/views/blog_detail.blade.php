@extends('layouts.general')
@section('page_name')
    Artikel
@endsection

@section('title')
    {{ $blog->title }}
@endsection

@section('content')
    <!-- Start of Blog single content
		============================================= -->
	<section id="blog-detail" class="blog-details-section">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="blog-details-content">
						<div class="post-content-details">
							<div class="blog-detail-thumbnile mb35">
								<img src="{{Storage::url($blog->banner_url)}}" alt="">
							</div>
							<h2>{{$blog->title}}.</h2>

							<div class="date-meta text-uppercase">
								<span><i class="fas fa-calendar-alt"></i> {{\Carbon\Carbon::parse($blog->created_at)->format('Y-m-d')}}</span>
								<span><i class="fas fa-user"></i> <a href="{{$blog->coach->slug}}">{{$blog->coach->name}}</a></span>
							</div>
							@if (!empty($blog->highlight))
								<h3 style="text-align:justify;">{{$blog->highlight}}</h3>
							@endif
							<div style="font-size: 20px; color: #333333; font-weight: 300;margin-bottom: 20px;">
								{!! $blog->body !!}
							</div>
						</div>
						<div class="author-comment">
							<div class="author-img">
								<img src="{{Storage::url($blog->coach->picture_url)}}" alt="">
							</div>
							<div class="author-designation-comment">
								<div style="text-transform:uppercase;">
									Penulis: <a href="{{$blog->coach->slug}}"><span>{{$blog->coach->name}}</span><a>
								</div>
								<p>
									{{$blog->coach->highlight}}
								</p>
								<br>
								<br>
							</div>
						</div>
						<div class="next-prev-post">
							@if (!empty($blog->previous))
							<div class="next-post-item float-left">
								<a href="{{$blog->previous->slug}}"><i class="fas fa-arrow-circle-left"></i>Sebelumnya</a>
							</div>
							@endif

							@if (!empty($blog->next))
							<div class="next-post-item float-right">
								<a href="{{$blog->next->slug}}">Selanjutnya<i class="fas fa-arrow-circle-right"></i></a>
							</div>
							@endif
						</div>
					</div>

					@if (!empty($blog->recommended))
					<div class="blog-recent-post about-teacher-2">
						<div class="section-title-2  headline text-left">
							<h2><span>Artikel</span> Lainnya</h2>
						</div>
						<div class="recent-post-item">
							<div class="row">
								@foreach ($blog->recommended as $blogRecommended)
									@if($loop->iteration > 2)
										@break
									@endif
									<div class="col-md-6">
										<a href="{{$blogRecommended->slug}}">
										<div class="blog-post-img-content">
											<div class="blog-img-date relative-position">
												<div class="blog-thumnile">
													<img src="{{Storage::url($blogRecommended->banner_url)}}" alt="">
												</div>
												<div class="course-price text-center gradient-bg">
													<span>{{\Carbon\Carbon::parse($blogRecommended->created_at)->format('Y-m-d')}}</span>
												</div>
											</div>
											<div class="blog-title-content headline">
												<h3>{{$blogRecommended->title}}.</h3>
											</div>
										</div>
										</a>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					@endif
				</div>

				<div class="col-md-3">
					<div class="side-bar">
						<div class="side-bar-search">
							<form action="{{route('blog')}}" method="get">
								<input type="text" name="keyword" class="" placeholder="Cari">
								<button type="submit"><i class="fas fa-search"></i></button>
							</form>
						</div>

						<div class="side-bar-widget">
							<h2 class="widget-title text-capitalize">Kategori <span>Artikel.</span></h2>
							<div class="post-categori ul-li-block">
								<ul>
									@foreach ($blogCategories as $blogCategory)
									<li class="cat-item"><a href="{{route('blog').'?category='.$blogCategory->id}}">{{$blogCategory->category}}</a></li>	
									@endforeach
								</ul>
							</div>
						</div>

						@if (!empty($blog->recommended))
						<div class="side-bar-widget">
							<h2 class="widget-title text-capitalize"><span>Artikel </span>Lainnya.</h2>
							<div class="latest-news-posts">
								@foreach ($blog->recommended as $blogRecommended)
								<a href="{{$blogRecommended->slug}}">
									<div class="latest-news-area">
										<div class="latest-news-thumbnile relative-position">
											<img src="{{Storage::url($blogRecommended->banner_url)}}" alt="">
											<div class="hover-search">
												<i class="fas fa-search"></i>
											</div>
											<div class="blakish-overlay"></div>
										</div>
										<div class="date-meta">
											<i class="fas fa-calendar-alt"></i> {{\Carbon\Carbon::parse($blogRecommended->created_at)->format('Y-m-d')}}
										</div>
										<h3 class="latest-title bold-font">{{$blogRecommended->title}}.</h3>
									</div>
									<!-- /post -->
								</a>
								@endforeach
								<div class="view-all-btn bold-font">
									<a href="{{route('blog')}}">Lihat Semua <i class="fas fa-chevron-circle-right"></i></a>
								</div>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End of Blog single content
		============================================= -->
@endsection