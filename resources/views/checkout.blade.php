@extends('layouts.general')
@section('page_name')
    Pesanan
@endsection

@section('title')
    Daftar Pesanan
@endsection

@section('content')
<!-- Start of Checkout content
============================================= -->
<section id="checkout" class="checkout-section">
    <div class="container">
        <div class="section-title mb45 headline text-center">
            <span class="subtitle text-uppercase">Pesanan</span>
            <h2>Lanjutkan <span>Pesanan Anda.</span></h2>
        </div>
        @if (count($carts)<1)
            Tidak ada pesanan.
        @else
        <div class="checkout-content">
            <div class="row">
                <div class="col-md-9">
                    <div class="order-item mb65 course-page-section">
                        <div class="section-title-2  headline text-left">
                            <h2>Daftar <span>Pesanan.</span></h2>
                        </div>

                        <div class="course-list-view table-responsive">
                            <table class="table">
                                
                                <thead>
                                    <tr class="list-head">
                                        <th>JUDUL Workshop</th>
                                        <th>KATEGORI</th>
                                        <th>LOKASI</th>
                                        <th>Tanggal Mulai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($carts as $cart)
                                    <tr>
                                        <td>
                                            <div class="course-list-img-text">
                                                <div class="course-list-img">
                                                    <img src="{{Storage::url($cart->course->banner_url)}}" alt="">
                                                </div>
                                                <div class="course-list-text">
                                                    <h3><a href="{{route('course_detail', [str_slug($cart->course->title), $cart->course->id])}}">{{$cart->course->title}}.</a></h3>
                                                    <div class="course-meta">
                                                        <span class="course-category bold-font"><a href="#">
                                                            @if (empty($cart->course->fee))
                                                                Gratis
                                                            @else
                                                                Rp{{number_format($cart->course->fee, 2, ',', '.')}}
                                                            @endif
                                                        </a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="course-type-list">
                                                <span>{{$cart->course->category->category}}</span>
                                            </div>
                                        </td>
                                        <td>{{$cart->course->venue}}</td>
                                        <td class="dlt-price relative-position">
                                            {{\Carbon\Carbon::parse($cart->course->start_date)->format('d-m-Y')}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="order-payment">
                        <div class="section-title-2  headline text-left">
                            <h2>Cara <span>Pembayaran.</span></h2>
                        </div>
                        <div class="payment-method">
                            <div class="payment-method-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="method-header-text">
                                            <div class="checkbox">
                                                <label>
                                                    Transfer Bank
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="payment-img float-right">
                                            <img src="" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="check-out-form">
                                <div class="payment-info">
                                    <label class=" control-label">BCA</label>
                                    <div>
                                        <img src="{{url('/').'/img/bank/bca.png'}}" alt="">
                                        <span> 912381238123 a/n PT. Lestari Budiman</span>    
                                    </div>
                                    
                                </div>
                                <div class="payment-info">
                                    <label class=" control-label">Mandiri</label>
                                    <div>
                                        <img src="{{url('/').'/img/bank/mandiri.png'}}" alt="">
                                        <span> 1231410908102131 a/n PT. Lestari Budiman</span>    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form id="order-form" method="POST" action="{{route('checkout.order')}}">
                            @csrf
                        </form>

                        <div class="genius-btn mt25 gradient-bg text-center text-uppercase  bold-font">
                            <a href="#" onclick="document.getElementById('order-form').submit();">Pesan Sekarang <i class="fas fa-caret-right"></i></a>
                        </div>
                        <div class="terms-text pb45 mt25">
                            {{-- <p>By confirming this purchase, I agree to the <b>Term of Use, Refund Policy</b>  and <b>Privacy Policy</b></p> --}}
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="side-bar-widget first-widget">
                        <h2 class="widget-title text-capitalize">Detail <span>Pesanan.</span></h2>
                        <div class="sub-total-item">
                            <span class="sub-total-title"></span>
                            <div class="purchase-list mt15 ul-li-block">
                                
                                <div class="in-total">TOTAL <span>
                                    @if (empty($cart->course->fee))
                                        Gratis
                                    @else
                                        Rp{{number_format($cart->course->fee, 2, ',', '.')}}
                                    @endif    
                                </span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
<!-- End  of Checkout content
============================================= -->
@endsection