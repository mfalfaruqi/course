@extends('layouts.general')
@section('page_name')
    Artikel
@endsection

@section('title')
    Artikel
@endsection

@section('content')
<!-- Start of blog content
============================================= -->
<section id="blog-item" class="blog-item-post">
	<div class="container">
		<div class="blog-content-details">
			<div class="row">
				<div class="col-md-9">
					<div class="blog-post-content">
						<div class="genius-post-item">
							<div class="tab-container">
								<div id="tab1" class="tab-content-1 pt35">
									<div class="row">
										@foreach ($blogs as $blog)
										<div class="col-md-6">
											<div class="blog-post-img-content">
												<div class="blog-img-date relative-position">
													<div class="blog-thumnile">
														<a href="{{$blog->slug}}"><img src="{{Storage::url($blog->banner_url)}}" alt="{{$blog->title}}"></a>
													</div>
													<div class="course-price text-center gradient-bg">
														<span>{{\Carbon\Carbon::parse($blog->created_at)->format('Y-m-d')}}</span>
													</div>
												</div>
												<div class="blog-title-content headline">
													<h3><a href="{{$blog->slug}}">{{$blog->title}}</a></h3>
													<div class="blog-content">
														{{str_limit($blog->highlight, 100)}}
													</div>

													<div class="view-all-btn bold-font">
														<a href="{{$blog->slug}}">Baca lebih lanjut <i class="fas fa-chevron-circle-right"></i></a>
													</div>
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div><!-- 1st tab -->
							</div>
						</div>


						<div class="couse-pagination text-center ul-li">
							@if (count($blogs) > 0) {!! $blogs->links() !!} @endif
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="side-bar">
						<div class="side-bar-search">
							<form action="{{route('blog')}}" method="get">
								<input type="text" name="keyword" class="" placeholder="Cari">
								<input type="hidden" name="category" value="{{app('request')->input('category')}}" class="" placeholder="Cari">
								<button type="submit"><i class="fas fa-search"></i></button>
							</form>
						</div>

						<div class="side-bar-widget">
							<h2 class="widget-title text-capitalize">Kategori <span>Artikel.</span></h2>
							<div class="post-categori ul-li-block">
								<ul>
									@foreach ($blogCategories as $blogCategory)
										<li class="cat-item"><a href="{{route('blog', ['category'=>$blogCategory->id])}}">{{$blogCategory->category}}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End of blog content
	============================================= -->
@endsection