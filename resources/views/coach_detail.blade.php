@extends('layouts.general')
@section('page_name')
    Educator
@endsection

@section('title')
    {{ $coach->name }}
@endsection

@section('content')
    <!-- Start of teacher details area
	============================================= -->	
	<section id="teacher-details" class="teacher-details-area">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="teacher-details-content mb45">
						<div class="row">
							<div class="col-md-6">
								<div class="teacher-details-img">
									@if (!empty($coach->picture_url))
										<img src="{{Storage::url($coach->picture_url)}}" alt="{{$coach->name}}">
									@else
										<img src="{{url('/')}}/img/teacher/default.jpg" alt="{{$coach->name}}">
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="teacher-details-text">
									<div class="section-title-2  headline text-left">
										<h2>{{$coach->name}}</h2>
										<div class="teacher-deg">
											<span>{{$coach->highlight}}</span> 
										</div>
									</div>
									<div class="teacher-desc-social ul-li">
										<ul>
											@if (!empty($coach->facebook))
											<li>
												<a href="{{$coach->facebook}}">
													<div class="info-social">
														<i class="fab fa-facebook-f"></i>
													</div>
													<span class="info-text">Facebook</span>
												</a>
											</li>
											@endif
											@if (!empty($coach->twitter))
											<li>
												<a href="{{$coach->twitter}}">
													<div class="info-social">
														<i class="fab fa-twitter"></i>
													</div>
													<span class="info-text">Twitter</span>
												</a>
											</li>
											@endif
											@if (!empty($coach->instagram))
											<li>
												<a href="{{$coach->instagram}}">
													<div class="info-social">
														<i class="fab fa-instagram"></i>
													</div>
													<span class="info-text">Instagram</span>

												</a>
											</li>
											@endif
										</ul>
									</div>

									<div class="teacher-address">
										<div class="address-details ul-li-block">
											<ul>
												@if (!empty($coach->address))
												<li>
													<div class="addrs-icon">
														<i class="fas fa-map-marker-alt"></i>
													</div>
													<div class="add-info">
														<span><b>Alamat: </b>{{$coach->address}}</span>
													</div>
												</li>
												@endif
												@if (!empty($coach->phone))
												<li>
													<div class="addrs-icon">
														<i class="fas fa-phone"></i>
													</div>
													<div class="add-info">
														<span><b>Telp: </b>{{$coach->phone}}</span>
													</div>
												</li>
												@endif
												@if (!empty($coach->email))
												<li>
													<div class="addrs-icon">
														<i class="fas fa-envelope"></i>
													</div>
													<div class="add-info">
														<span><b>E-mail: </b>{{$coach->email}}</span>
													</div>
												</li>
												@endif
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="about-teacher mb45">
						<div class="section-title-2  headline text-left">
							<h2>Tentang <span>Educator.</span></h2>
						</div>
						<p>
							{!! $coach->description !!}
						</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="side-bar-widget">
						<h2 class="widget-title text-capitalize"><span>Workshop </span>Terbaru.</h2>
						<div class="latest-news-posts">
							@foreach ($courses as $course)
							<div class="latest-news-area">
								<div class="latest-news-thumbnile relative-position">
									@if (empty($course->banner_url))
										<img src="{{url('/')}}/img/course/default.jpg" alt="">
									@else
										<img src="{{Storage::url($course->banner_url)}}" alt="{{$course->title}}">
									@endif 
									<div class="hover-search">
										<i class="fas fa-search"></i>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="date-meta">
									<i class="fas fa-calendar-alt"></i> {{$course->start_date}}
								</div>
								<h3 class="latest-title bold-font"><a href="{{$course->slug}}">{{$course->title}}</a></h3>
							</div>
							<!-- /post -->
							@endforeach
							<div class="view-all-btn bold-font">
								<a href="{{route('course')}}">Lihat Semua <i class="fas fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End  of teacher details area
		============================================= -->
@endsection