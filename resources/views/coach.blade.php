@extends('layouts.general')
@section('page_name')
    Educator
@endsection

@section('title')
    Educator
@endsection

@section('content')
    <!-- Start of teacher section
		============================================= -->
		<section id="teacher-page" class="teacher-page-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="teachers-archive">
							<div class="row">
								@if (count($coaches) < 1) 
									<h3>Data tidak ditemukan</h3>
								@endif
								@foreach ($coaches as $coach)
								<div class="col-md-4 col-sm-6">
									<div class="teacher-pic-content">
										<div class="teacher-img-content relative-position">
											@if (!empty($coach->picture_url))
												<img src="{{Storage::url($coach->picture_url)}}" alt="{{$coach->name}}">
											@else
												<img src="{{url('/')}}/img/teacher/default.jpg" alt="{{$coach->name}}">
											@endif
											<div class="teacher-hover-item">
												<div class="teacher-social-name ul-li-block">
													<ul>
														@if (!empty($coach->facebook))<li><a href="{{ $coach->facebook }}"><i class="fab fa-facebook-f" title="Facebook"></i></a></li>@endif
														@if (!empty($coach->twitter))<li><a href="{{ $coach->twitter}}"><i class="fab fa-twitter" title="Twitter"></i></a></li>@endif
														@if (!empty($coach->instagram))<li><a href="{{ $coach->instagram}}"><i class="fab fa-instagram" title="Instagram"></i></a></li>@endif
													</ul>
												</div>
												<div class="teacher-text">
													{{$coach->name}}
												</div>
											</div>
											<div class="teacher-next text-center">
												<a href="{{ $coach->slug }}"><i class="text-gradiant fas fa-arrow-right"></i></a>
											</div>
										</div>
										<div class="teacher-name-designation">
											<a href="{{$coach->slug}}"><span class="teacher-name">{{$coach->name}}</span></a>
											<span class="teacher-designation">{{$coach->highlight}}</span>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="couse-pagination text-center ul-li">
								@if (count($coaches) > 0) {!! $coaches->links() !!} @endif
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="side-bar">
							<div class="side-bar-widget first-widget">
								<h2 class="widget-title text-capitalize"><span>Cari </span>Motivatormu.</h2>
								<div class="listing-filter-form pb30">
									<form action="{{ Request::url() }}" id="search" method="get">
										<div class="filter-search mb20">
											<label>Kata Kunci</label>
											<input type="text" name="keyword" placeholder="Siapa yang kamu cari?" value="{{ Request::get('keyword') }}">
										</div>
										<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
											<a href="#" onclick="document.getElementById('search').submit();">CARI <i class="fas fa-caret-right"></i></a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End of teacher section
			============================================= -->
@endsection