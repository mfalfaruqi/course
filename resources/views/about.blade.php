@extends('layouts.general')
@section('page_name')
    Tentang Kami
@endsection

@section('title')
    Tentang Kami
@endsection

@section('content')
    <!-- Start of about us content
    ============================================= -->
    <section id="about-page" class="about-page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="about-us-content-item">
                            <div class="about-gallery">
                                <div class="about-gallery-img grid-1">
                                    <img src="{{url('/')}}/img/about/abt-wg-1.jpg" alt="">
                                </div>
                                <div class="about-gallery-img grid-2">
                                    <img src="{{url('/')}}/img/about/abt-wg-2.jpg" alt="">
                                </div>
                                <div class="about-gallery-img grid-2">
                                    <img src="{{url('/')}}/img/about/abt-wg-3.jpg" alt="">
                                </div>
                            </div>
                            <!-- /gallery -->

                            <div class="about-text-item">
                                <div class="section-title-2  headline text-left">
                                    <h2>We Are <span>WGrow.id,</span> Your Growing Enabler</h2>
                                </div>
                                <p>
                                    Perubahan zaman yang begitu cepatnya dan perkembangan teknologi yang pesat menyebabkan sebuah kondisi yang sering disebut sebagai VUCA. VUCA yang merupakan singkatan dari Volatile (bergejolak), Uncertain (tidak pasti), Complex (kompleks), dan Ambigue (tidak jelas) merupakan gambaran situasi di dunia bisnis di masa kini yang penuh dengan ketidakpastian. Hal ini karena para pelaku bisnis saat ini sendiri masih cenderung menebak-nebak apa perubahan yang akan terjadi.
                                </p>
                                <p>
                                    Indonesia sendiri merupakan salah satu Negara yang mulai merasakan dampak dari VUCA dimana bermunculan model bisnis baru yang sering disebut sebagai startup yang menggunakan teknologi didalam menjalankan bisnisnya.
                                </p>
                                <p>
                                    Perubahan landscape bisnis ini sendiri seharusnya diimbangi dengan kesiapan sumber daya manusianya. Namun sampai saat ini masih terlihat belum siapnya sumber daya manusia Indonesia dalam kompetisi global. Padahal perubahan bentuk profesi-profesi baru yang lebih dinamis tentunya membutuhkan kapasitas sumber daya manusia yang lebih kreatif dan inovatif sehingga bisa tetap relevan dengan jaman. Disini program pengembangan diri pun harus disusun sedemikian rupa sehingga mampu memenuhi tantangan perubahan. Sayangnya, program pengembangan diri yang seringkali ada pun belum banyak yang mampu menjawab tantangan tersebut. Disinilah WGrow hadir untuk mengisi celah kekosongan yang ada dan menghadirkan program-program yang terupdate dan sesuai dengan perkembangan generasi milenials.
                                </p>
                                <p>
                                    Tidak sekedar terupdate, namun program-program yang kami tawarkan baik secara offline atau online didalam platform kami pun dirancang secara terstruktur dan didesain sedemikian rupa untuk meningkatkan pengalaman pembelajaran yang tidak hanya fun, melainkan juga relevan dengan dunia bisnis saat ini.
                                </p>
                            </div>
                            <!-- /about-text -->

                            <div class="about-text-item">
                                <div class="section-title-2  headline text-left">
                                    <h2>Visi <span>WGrow.id</span></h2>
                                </div>
                                <p>
                                    <i>"Meningkatkan kualitas kehidupan masyarakat indonesia melalui penyediaan akses program pengembangan diri yang tepat dan berkualitas."</i>
                                </p>
                            </div>

                            <div class="about-text-item">
                                <div class="section-title-2  headline text-left">
                                    <h2>Misi <span>WGrow.id</span></h2>
                                </div>
                                <div >
                                <p style="line-height: 150%;">
                                    - Menjadi penghubung educator dengan user atau perusahaan yang membutuhkan jasanya.<br>
                                    - Membantu user menemukan program yang sesuai potensi dan kebutuhannya.<br>
                                    - Meningkatkan kualitas educator sehingga mampu menjadi solusi di masyarakat.
                                </p>
                                </div>
                            </div>

                            <div class="course-teacher about-teacher-2">
                                <div class="course-advantage about-teacher-2">
                                    <div class="section-title-2  headline text-left">
                                        <h2><span>WGrow.id</span> Services</h2>
                                    </div>
                                    <div class="service-slide_3">
                                        <div class="service-text-icon">
                                            <div class="service-icon float-left">
                                                <i class="text-gradiant flaticon-011-certificate"></i>
                                            </div>
                                            <div class="service-text">
                                                <h3 class="bold-font">Educators.</h3>
                                                <p>WGrow.id tergabung lebih dari 115 Educators yang memiliki spesialisasi di bidangnya.</p>
                                            </div>
                                        </div>
                                        <div class="service-text-icon">
                                            <div class="service-icon float-left">
                                                <i class="text-gradiant flaticon-025-presentation"></i>
                                            </div>
                                            <div class="service-text">
                                                <h3 class="bold-font">Speaker.</h3>
                                                <p>WGrow.id menghadirkan speaker dari praktisi dan pemilik bisnis langsung.</p>
                                            </div>
                                        </div>
                                        <div class="service-text-icon">
                                            <div class="service-icon float-left">
                                                <i class="text-gradiant flaticon-030-training"></i>
                                            </div>
                                            <div class="service-text">
                                                <h3 class="bold-font">Event Management.</h3>
                                                <p>WGrow.id membantu dalam kebutuhan acara pelatihan.</p><br>
                                            </div>
                                        </div>
                                        <div class="service-text-icon">
                                            <div class="service-icon float-left">
                                                <i class="text-gradiant flaticon-005-benefit"></i>
                                            </div>
                                            <div class="service-text">
                                                <h3 class="bold-font">Digital Marketing.</h3>
                                                <p>WGrow.id mengembangkan branding melalui media digital.</p><br>
                                            </div>
                                        </div>
                                        <div class="service-text-icon">
                                            <div class="service-icon float-left">
                                                <i class="text-gradiant flaticon-012-handshake"></i>
                                            </div>
                                            <div class="service-text">
                                                <h3 class="bold-font">Event Activity.</h3>
                                                <p>WGrow.id mengadakan program campaign yang melibatkan komunitas.</p>
                                            </div>
                                        </div>
                                        <div class="service-text-icon">
                                            <div class="service-icon float-left">
                                                <i class="text-gradiant flaticon-013-chart"></i>
                                            </div>
                                            <div class="service-text">
                                                <h3 class="bold-font">Portal Media.</h3>
                                                <p>WGrow.id mengedukasi melalui artikel dan tulisan yang dikemas menarik.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /course-advantage -->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="side-bar-widget first-widget">
                            {{-- <h2 class="widget-title text-capitalize"><span>Kami </span> adalah...</h2> --}}
                            <div class="course-img">
                                <img src="{{url('/')}}/img/logo/logo-wg.jpg" alt="">
                            </div>
                            {{-- <div class="course-desc">
                                <p>Lorem ipsum dolor sit  consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                            </div>
                            <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
                                <a href="#">VIEW ONLINE COURSES <i class="fas fa-caret-right"></i></a>
                            </div> --}}
                        </div>

                        {{-- <div class="side-bar-widget">
                            <h2 class="widget-title text-capitalize"><span>Related </span>News.</h2>
                            <div class="latest-news-posts">
                                <div class="latest-news-area">
                                    <div class="latest-news-thumbnile relative-position">
                                        <img src="{{url('/')}}/img/blog/lb-1.jpg" alt="">
                                        <div class="hover-search">
                                            <i class="fas fa-search"></i>
                                        </div>
                                        <div class="blakish-overlay"></div>
                                    </div>
                                    <div class="date-meta">
                                        <i class="fas fa-calendar-alt"></i> 26 April 2018
                                    </div>
                                    <h3 class="latest-title bold-font"><a href="#">Affiliate Marketing A Beginner’s Guide.</a></h3>
                                </div>
                                <!-- /post -->

                                <div class="latest-news-posts">
                                    <div class="latest-news-area">
                                        <div class="latest-news-thumbnile relative-position">
                                            <img src="{{url('/')}}/img/blog/lb-2.jpg" alt="">
                                            <div class="hover-search">
                                                <i class="fas fa-search"></i>
                                            </div>
                                            <div class="blakish-overlay"></div>
                                        </div>
                                        <div class="date-meta">
                                            <i class="fas fa-calendar-alt"></i> 26 April 2018
                                        </div>
                                        <h3 class="latest-title bold-font"><a href="#">No.1 The Best Online Course 2018.</a></h3>
                                    </div>
                                    <!-- /post -->
                                </div>

                                <div class="view-all-btn bold-font">
                                    <a href="#">View All News <i class="fas fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="side-bar-widget">
                            <h2 class="widget-title text-capitalize"><span>Featured</span> Course.</h2>
                            <div class="featured-course">
                                <div class="best-course-pic-text relative-position">
                                    <div class="best-course-pic relative-position">
                                        <img src="{{url('/')}}/img/blog/fb-1.jpg" alt="">
                                        <div class="trend-badge-2 text-center text-uppercase">
                                            <i class="fas fa-bolt"></i>
                                            <span>Trending</span>
                                        </div>
                                    </div>
                                    <div class="best-course-text">
                                        <div class="course-title mb20 headline relative-position">
                                            <h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
                                        </div>
                                        <div class="course-meta">
                                            <span class="course-category"><a href="#">Web Design</a></span>
                                            <span class="course-author"><a href="#">250 Students</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </section>
    <!-- End of about us content
        ============================================= -->

@endsection