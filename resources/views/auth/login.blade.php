@extends('layouts.general')
@section('page_name')
    Masuk
@endsection
@section('title')
    Masuk
@endsection
@section('additional_css')
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/login.css') }}">
@endsection
@section('additional_js')
    <link rel="stylesheet" href="{{ mix('/js/login.js') }}">
@endsection

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" id="form-login" action="{{ route('login') }}" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    @csrf
                    <span class="login100-form-title">
                        <img src="{{url('/')}}/img/logo/new-logo-black.png" alt="logo" class="img-logo">
                        <span>Your Growing Enabler</span>
                    </span>

                    <div class="wrap-input100 validate-input m-b-16" data-validate="Masukan E-Mail Anda">
                        <input class="input100" type="email" name="email" placeholder="E-Mail" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Masukan Kata Sandi">
                        <input class="input100" type="password" name="password" placeholder="Kata Sandi" required>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input">
                        <label class="label100">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Ingat Saya') }}
                        </label>
                    </div>

                    <div class="text-right p-t-13 p-b-23">
                        <span class="txt1">
                            Lupa
                        </span>

                        <a href="{{ route('password.request') }}" class="txt2">
                            kata sandi?
                        </a>
                    </div>

                    <div class="login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            Masuk
                        </button>
                    </div>

                    <div class="flex-col-c p-t-100 p-b-40">
                        <span class="txt1 p-b-9">
                            Belum punya akun?
                        </span>

                        <a href="{{ route('register') }}" class="txt3">
                            Daftar Sekarang!
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
