@extends('layouts.general')
@section('page_name')
    Ganti Kata Sandi
@endsection
@section('title')
    Ganti Kata Sandi
@endsection
@section('additional_css')
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/login.css') }}">
@endsection
@section('additional_js')
    <link rel="stylesheet" href="{{ mix('/js/login.js') }}">
@endsection

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" id="form-login" action="{{ route('dashboard.change_password.submit') }}" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    @csrf
                    <span class="login100-form-title">
                        <img src="{{url('/')}}/img/logo/new-logo-black.png" alt="logo" class="img-logo">
                        <span>Your Growing Enabler</span>
                    </span>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="old_password" placeholder="Kata Sandi Lama" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="new_password" placeholder="Kata Sandi Baru" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="new_password_confirmation" placeholder="Konfirmasi Kata Sandi Baru" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="register100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            Ganti Kata Sandi
                        </button>
                    </div>

                    <div class="flex-col-c p-t-100 p-b-40">
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
