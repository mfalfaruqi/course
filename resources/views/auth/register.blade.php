@extends('layouts.general')
@section('page_name')
    Daftar
@endsection
@section('title')
    Daftar
@endsection
@section('additional_css')
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/login.css') }}">
@endsection
@section('additional_js')
    <link rel="stylesheet" href="{{ mix('/js/login.js') }}">
@endsection

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" id="form-register" action="{{ route('register') }}" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    @csrf
                    <span class="login100-form-title">
                        <img src="{{url('/')}}/img/logo/new-logo-black.png" alt="logo" class="img-logo">
                        <span>Your Growing Enabler</span>
                    </span>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="text" name="name" placeholder="Nama Lengkap" value="{{ old('name') }}" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="email" name="email" placeholder="E-Mail" value="{{ old('email') }}" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="password" placeholder="Kata Sandi" required>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="password_confirmation" placeholder="Konfirmasi Kata Sandi" required>
                        <span class="focus-input100"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="text" name="phone" placeholder="Nomor Telepon" value="{{ old('phone') }}">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="text" name="address" placeholder="Alamat" value="{{ old('address') }}">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="text" name="occupation" placeholder="Pekerjaan" value="{{ old('occupation') }}">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="register100-form-btn">
                        <button type="submit" class="login100-form-btn">
                                Daftar Sekarang!
                        </button>
                    </div>

                    <div class="flex-col-c p-t-100 p-b-40">
                        <span class="txt1 p-b-9">
                            Sudah punya akun?
                        </span>

                        <a href="{{ route('login') }}" class="txt3">
                            Masuk
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
