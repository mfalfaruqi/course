@extends('layouts.general')
@section('page_name')
    Ganti Kata Sandi
@endsection
@section('title')
    Ganti Kata Sandi
@endsection
@section('additional_css')
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/login.css') }}">
@endsection
@section('additional_js')
    <link rel="stylesheet" href="{{ mix('/js/login.js') }}">
@endsection

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form method="POST" id="form-login" action="{{ route('password.update') }}" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    @csrf
                    <span class="login100-form-title">
                        <img src="{{url('/')}}/img/logo/new-logo-black.png" alt="logo" class="img-logo">
                        <span>Your Growing Enabler</span>
                    </span>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="email" name="email" placeholder="Email" value="{{ $email ?? old('email') }}" required>
                        <span class="focus-input100"></span>
                    </div>

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="password" placeholder="Kata Sandi" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16">
                        <input class="input100" type="password" name="password_confirmation" placeholder="Konfirmasi Kata Sandi" required>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="register100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            Ganti Kata Sandi
                        </button>
                    </div>

                    <div class="flex-col-c p-t-100 p-b-40">
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
{{-- <section id="reset-password-request-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Ganti Kata Sandi') }}</div>

                    <div class="card-body">
                        <form method="POST" id="form-reset-password" action="{{ route('password.update') }}">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Kata Sandi') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Konfirmasi Kata Sandi') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <div class="genius-btn gradient-bg text-center text-uppercase float-left bold-font">
                                        <a href="#" onclick="document.getElementById('form-reset-password').submit()">Simpan</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
@endsection
