@extends('layouts.general')
@section('page_name')
    Workshop
@endsection

@section('title')
    {{ $course->title }}
@endsection

@section('content')
    <!-- Start of course details section
		============================================= -->
		<section id="course-details" class="course-details-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="course-details-item">
							<div class="course-single-pic mb30">
								@if (empty($course->banner_url))
									<img src="{{url('/')}}/img/course/default.jpg" alt="">
								@else
									<img src="{{Storage::url($course->banner_url)}}" alt="{{$course->title}}">
								@endif 
							</div>
							<div class="course-single-text">
								<div class="course-title mt10 headline relative-position">
									<h3><b>{{$course->title}}</b></h3>
								</div>
								<div class="course-details-content">
									{!! $course->description !!}
								</div>
							</div>
						</div>
						<!-- /course-details -->
					</div>

					<div class="col-md-3">
						<div class="side-bar">
							@if ($course->course_type_id == 1)
							<div class="course-side-bar-widget">
								<h3>Harga/Seat <br>
									<span>
										@if (empty($course->fee))
											Gratis
										@else
											Rp{{number_format($course->fee, 2, ',', '.')}}
										@endif
									</span>
								 </h3>
								 <form id="order-form" method="POST" action="{{route('add_to_cart')}}">
									@csrf
									<input type="hidden" name="course_id" value="{{$course->id}}">
								</form>
								<div class="genius-btn gradient-bg text-center text-uppercase float-left bold-font">
									<a href="#" onclick="document.getElementById('order-form').submit();">Daftar Sekarang <i class="fas fa-caret-right"></i></a>
								</div>
							</div>
							@endif
							<div class="enrolled-student">
								
							</div>
							<div class="couse-feature ul-li-block">
								<ul>
									<li>Tanggal <span>{{\Carbon\Carbon::parse($course->start_date)->format('d/m/Y')}}</span></li>
									@if(!empty($course->end_date && $course->start_date != $course->end_date))
										<li>Selesai <span>{{\Carbon\Carbon::parse($course->end_date)->format('d/m/Y')}}</span></li>
									@endif
									<li>Waktu <span>{{\Carbon\Carbon::parse($course->start_time)->format('H:i')}} @if (!empty($course->end_time))- {{\Carbon\Carbon::parse($course->end_time)->format('H:i')}} @endif WIB</span></li>
									<li>Lokasi  <span><br>{{ $course->venue }}, {{$course->venue_address}}</span></li>
									@if (count($course->coaches)>0)
										<li>
											Educator <span><br>@foreach ($course->coaches as $key=>$coach)
												<a class="btn-link" href="{{$coach->slug}}"> {{$coach->name}}</a>
												@if ($key < count($course->coaches)-1), @endif
											@endforeach</span>
										</li>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- End of course details section
		============================================= -->	
@endsection