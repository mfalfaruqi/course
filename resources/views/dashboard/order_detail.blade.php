@extends('layouts.general')
@section('page_name')
    Masuk
@endsection

@section('content')
<section id="login-page" class="about-page-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Pesanan') }}</div>

                    <div class="card-body">
                        <form method="POST" id="form-order" action="{{ route('dashboard.order.update', $order->id) }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Nomor Pesanan') }}</label>

                                <div class="col-md-6">
                                    <input class="form-control disabledInput" value="{{ $order->order_number }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Status Pesanan') }}</label>

                                <div class="col-md-6">
                                    <input class="form-control disabledInput" value="{{ $order->orderStatus->order_status }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Pembayaran') }}</label>

                                <div class="col-md-6">
                                    <input class="form-control disabledInput" value="Rp{{number_format($order->invoice_price, 2, ',', '.')}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Bukti Pembayaran') }}</label>

                                <div class="col-md-6">
                                        
                                    @if (!empty($order->acquittance_url))
                                        <a href="{{ Storage::url($order->acquittance_url)}}" id="acquittance-url-value" target="_blank"><img src="{{ Storage::url($order->acquittance_url)}}" style="max-width:100%; height:auto;"></a>
                                        <input id="acquittance-url-input" type="file" name="acquittance" class="form-control" style="display: none;" disabled>
                                        @if ($order->order_status_id != 3)
                                            <div class="input-group-append">
                                                <button id="reupload-button" class="btn btn-outline-secondary btn-sm" type="button">Upload Ulang</button>
                                            </div>
                                        @endif

                                        <script>
                                            $('#reupload-button').click(function(){
                                                $('#acquittance-url-value').hide();
                                                $('#acquittance-url-input').show();
                                                $('#acquittance-url-input').removeAttr("disabled")
                                                $('#reupload-button').hide();
                                            });
                                        </script>
                                    @else
                                        <input type="file" name="acquittance" class="form-control">
                                    @endif
                                    
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4" style="line-height: 50px;">
                                    <div class="genius-btn gradient-bg text-center text-uppercase float-left bold-font">
                                        <a href="#" onclick="document.getElementById('form-order').submit()">Simpan</a>
                                    </div>
                                    <a class="btn btn-link" href="{{ route('dashboard') }}">
                                        Batal
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Detail Pesanan') }}</div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Workshop') }}</label>

                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->course->title }}</span>
                            </div>
                        </div>    
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Mulai') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ Carbon\Carbon::parse($order->course->start_date . $order->course->start_time) }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Selesai') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ Carbon\Carbon::parse($order->course->end_date . $order->course->end_time) }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Lokasi') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->course->venue }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Alamat Lokasi') }}</label>
                            <div class="col-md-6">
                                <span class="form-control disabledInput" >{{ $order->course->venue_address }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>
                            <div class="col-md-6">
                                <a class="btn btn-sm btn-light" href="{{ route('course_detail', [str_slug($order->course->title), $order->course->id]) }}">Klik di sini</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection