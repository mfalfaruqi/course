@extends('layouts.general')
@section('page_name')
    Workshop
@endsection

@section('title')
    Workshop
@endsection

@section('content')
    <!-- Start of course section
		============================================= -->
		<section id="course-page" class="course-page-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="genius-post-item">
							<div class="tab-container">
								<div id="tab1" class="tab-content-1 pt35">
									<div class="best-course-area best-course-v2">
										<div class="row">
											@if (count($courses) < 1) 
												<h3>Data tidak ditemukan</h3>
											@endif
											@foreach ($courses as $course)
											<div class="col-md-6">
												<div class="best-course-pic-text relative-position">
													<div class="best-course-pic relative-position">
														@if (empty($course->banner_url))
															<img src="{{url('/')}}/img/course/default.jpg" alt="">
														@else
															<img src="{{Storage::url($course->banner_url)}}" alt="{{$course->title}}">
														@endif 
														@if ($course->course_type_id == 1)
															<div class="course-price text-center gradient-bg"  style="position:initial; border-radius:0px;">
																@if (!empty($course->fee))
																	<span>Rp{{number_format($course->fee, 2, ',', '.')}}</span>
																@else
																	<span>Gratis</span>
																@endif
															</div>
														@else
															<div class="course-price text-center gradient-bg" style="position:initial; border-radius:0px;">
																<span>Contact Us For Detail</span>
															</div>
														@endif
														<div class="course-details-btn">
															<a href="{{ $course->slug }}">DETAIL <i class="fas fa-arrow-right"></i></a>
														</div>
														<div class="blakish-overlay"></div>
													</div>
													<div class="best-course-text">
														<div class="course-title mb20 headline relative-position">
															<h3><a href="{{ $course->slug }}">{{ $course->title }}</a></h3>
														</div>
														<div class="course-meta">
															@if (!empty($course->category))
																<span class="course-category"><a href="{{Request::url().'?category='.$course->category->id}}">@if(!empty($course->category)){{ $course->category->category }}@endif</a></span>	
															@endif
														</div>
													</div>
												</div>
											</div>
											@endforeach
											<!-- /course -->
										</div>
									</div>
								</div><!-- /tab-1 -->
							</div>
						</div>

						<div class="couse-pagination text-center ul-li">
							@if (count($courses) > 0) {!! $courses->links() !!} @endif
						</div>
					</div>

					<div class="col-md-3">
						<div class="side-bar">
							<div class="side-bar-widget first-widget">
								<h2 class="widget-title text-capitalize"><span>Cari </span>Motivasimu.</h2>
								<div class="listing-filter-form pb30">
									<form action="{{ Request::url() }}" id="search" method="get">
										<div class="filter-select mb20">
											<label>KATEGORI</label>
											<select name="category">
												<option value="0">Semua Kategori</option>
												@foreach ($categories as $category)
													<option value="{{ $category->id }}" @if (Request::get('category') == $category->id) selected="" @endif>{{ $category->category }}</option>
												@endforeach
											</select>
										</div>
										<div class="filter-search mb20">
											<label>Kata Kunci</label>
											<input type="text" name="keyword" class="" placeholder="Apa yang kamu cari?" value="{{ Request::get('keyword') }}">
										</div>
										<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
											<a href="#" onclick="document.getElementById('search').submit();">CARI <i class="fas fa-caret-right"></i></a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- End of course section
		============================================= -->

	<!-- Start of best product section
    ============================================= -->
    <!-- @include('components.product') -->
    <!-- End  of best product section
    ============================================= -->

@endsection