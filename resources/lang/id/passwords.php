<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password minimal 6 karakter dan harus sesuai dengan password confirmation .',
    'reset' => 'Password Anda berhasil diperbarui!',
    'sent' => 'Kami telah mengirim E-Mail untuk reset password Anda! Silahkan cek E-Mail Anda.',
    'token' => 'Token reset password tidak valid.',
    'user' => "Alamat E-Mail tidak terdaftar.",

];
