<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coach extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'highlight', 'description', 'picture_url', 'address', 'phone', "email", "facebook", "twitter", "instagram"
    ];
}
