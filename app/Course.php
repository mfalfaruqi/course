<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;
    
    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function coaches() {
        return $this->belongsToMany('App\Coach', 'course_coaches');
    }

    public function orders() {
        return $this->hasMany('App\Order');
    }
}
