<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function orderStatus() {
        return $this->belongsTo('App\OrderStatus');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
