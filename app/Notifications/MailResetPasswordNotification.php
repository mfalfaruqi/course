<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordNotification extends Notification
{
    use Queueable;
    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    
    public function __construct($t)
    {
        //
        $this->token = $t;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject(env('APP_NAME').' - Permintaan Penggantian Kata Sandi')
                ->line('Anda menerima E-Mail ini karena kami mendapatkan permintaan penggantian kata sandi atas Akun Anda.')
                ->action('Ganti Kata Sandi', url(config('app.url').route('password.reset', $this->token, false)))
                ->line('Permintaan penggantian kata sandi ini akan berakhir dalam '.config('auth.passwords.users.expire').' menit.')
                ->line('Jika Anda tidak melakukan permintaan penggantian kata sandi ini, harap hiraukan E-Mail ini.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
