<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Datatables;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Validator;


class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index() {
        return view('dashboard.order');
    }

    public function changePasswordIndex() {
        return view('auth.change_password');
    }

    public function changePassword(Request $request) {
        $attributes = [
            'old_password' => 'Kata Sandi Lama',
            'new_password' => 'Kata Sandi Baru',
            'new_password_confirmation' => 'Konfirmasi Kata Sandi Baru'
        ];
        
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string|min:6',
            'new_password' => 'required|string|min:6|confirmed',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $email = Auth::user()->email;
        if (!Auth::attempt(['email'=>$email, 'password'=>$request->get('old_password')])) {
            return redirect()->back()->withErrors(['old_password'=>'Kata Sandi Lama tidak sesuai']);
        }
        
        $user = User::findOrFail(Auth::user()->id);
        $user->password = Hash::make($request->get('new_password'));
        $user->save();

        return redirect()->route('dashboard.orders')->with('status', "Kata Sandi Baru berhasil di simpan");
    }

    public function getOrders()
    {
        $query = Order::query();
        if (Auth::user()->user_type_id == 2) {
            $query = $query->where('user_id', Auth::user()->id);
        }
        return DataTables::of($query)
            ->addColumn('course_title', function (Order $order) {
                return $order->course->title;
            })
            ->addColumn('course_start', function (Order $order) {
                return Carbon::parse($order->course->start_date . $order->course->start_time);
            })
            ->addColumn('detail_url', function (Order $order) {
                return route('dashboard.order.detail', $order->id);
            })
            ->addColumn('status', function (Order $order) {
                return $order->orderStatus->order_status;
            })
            ->toJson();
    }

    public function detail(Request $request, $id) {
        $order = Order::findOrFail($id);

        if ($order->user_id != Auth::user()->id) {
            return redirect()->route('dashboard');
        }

        return view('dashboard.order_detail', compact('order'));
    }

    public function update(Request $request, $id) {
        // Validate
        $attributes = [
            'acquittance' => 'Bukti Pembayaran',
        ];
        
        $validator = Validator::make($request->all(), [
            'acquittance' => 'file|mimes:jpeg|max:3000', // max 3MB
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Find order
        $order = Order::findOrFail($id);

        if ($order->user_id != Auth::user()->id) {
            return redirect()->route('dashboard');
        }

        if ($request->file('acquittance') != null) {
            $uploadedFile = $request->file('acquittance');
            $path = $uploadedFile->store('public/img/acquittance');
            
            if (!empty($order->acquittance_url)) {
                // remove previous file
                Storage::delete($order->acquittance_url);
            }

            // save path
            $order->acquittance_url = $path;

            // set status
            if ($order->order_status_id == 1) {
                $order->order_status_id = 2;
            }
            $order->save();
        }

        return redirect()->route('dashboard.orders')->with('status', "Data pesanan ".$order->course->title." berhasil di simpan");
    }
}
