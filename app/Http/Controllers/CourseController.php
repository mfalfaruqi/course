<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Category;

class CourseController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function showAll(Request $request) {
        $courseModel = new Course();

        if ($request->get('category')>0) {
            $courseModel = $courseModel->where('category_id', '=', $request->get('category'));
        }

        if ($request->get('keyword') != "") {
            $courseModel = $courseModel->where('title', 'like', '%'.$request->get('keyword').'%');
        }
        
        $courses = $courseModel->orderBy('start_date', 'desc')->paginate(10);

        foreach ($courses as $course) {
            $course->slug = route('course').'/'.str_slug($course->title, '-').'-'.$course->id.'.html';
        }
        
        $categoryModel = new Category();
        $categories = $categoryModel->all();
        return view('course', compact('courses', 'categories'));
    }

    public function detail(Request $request, $slug, $id) {
        $courseModel = new Course();
        $course = $courseModel->findOrFail($id);
        foreach ($course->coaches as $coach) {
            $coach->slug = route('coach').'/'.str_slug($coach->name, '-').'-'.$coach->id.'.html';
        }
        
        return view('course_detail', compact('course'));
    }
}
