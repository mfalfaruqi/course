<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Banner;
use App\Coach;
use App\Course;
use App\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banners  = Banner::where('fg_active', true)
            ->orderBy('position')
            ->get();

        $coaches = Coach::orderBy('position')->get();
        foreach ($coaches AS $coach) {
            $coach->slug = route('coach').'/'.str_slug($coach->name, '-').'-'.$coach->id.'.html';
        }

        $blogModel = new Blog();
        $blogs = $blogModel->orderBy('created_at', 'desc')->limit(6)->get();
        foreach ($blogs as $blog) {
            $blog->slug = route('blog_detail', ['slug' => str_slug($blog->title, '-'), 'id' => $blog->id]);
        }
        
        $courses = Course::orderBy('created_at', 'desc')->limit(6)->get();
        foreach ($courses as $course) {
            $course->slug = route('course_detail', ['slug' => str_slug($course->title, '-'), 'id' => $course->id]);
        }

        return view('homepage', compact('banners', 'coaches', 'blogs', 'courses'));
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function test(Request $request) {
        dd($request);
    }
}
