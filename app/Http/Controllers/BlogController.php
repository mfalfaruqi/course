<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Coach;
use App\Blog;
use App\BlogCategory;

class BlogController extends Controller
{
    public function showAll(Request $request) {
        $blogModel = new Blog();

        $keyword = $request->get('keyword');
        if ($keyword != '') {
            $blogModel = $blogModel->where('title', 'like', '%'.$keyword.'%');
        }
        $category = $request->get('category');
        if (!empty($category)) {
            $blogModel = $blogModel->where('blog_category_id', $category);
        }

        $blogs = $blogModel->orderBy('created_at', 'desc')->paginate(10);

        foreach ($blogs as $blog) {
            $blog->slug = route('blog_detail', ['slug' => str_slug($blog->title, '-'), 'id' => $blog->id]);
        }

        // get blog category
        $blogCategoryModel = new BlogCategory();
        $blogCategories = $blogCategoryModel->orderBy('category')->get();

        return view('blog', compact('blogs', 'blogCategories'));
    }

    public function detail(Request $request, $slug, $id) {
        // get current blog  
        $blogModel = new Blog();
        $blog = $blogModel->findOrFail($id);
        $blog->coach->slug = route('coach_detail', ['slug'=>str_slug($blog->coach->name, '-'), 'id'=>$blog->coach->id]);

        // get previous blog
        $blog->previous = $blogModel->find($id - 1);
        if (!empty($blog->previous)) {
            $blog->previous->slug = route('blog_detail', ['slug'=>str_slug($blog->previous->title, '-'), 'id'=>$blog->previous->id]);
        }
        // get next blog
        $blog->next = $blogModel->find($id + 1);
        if (!empty($blog->next)) {
            $blog->next->slug = route('blog_detail', ['slug'=>str_slug($blog->next->title, '-'), 'id'=>$blog->next->id]);
        }

        // get recommended blog
        $blog->recommended = $blogModel->whereNotIn('id', [$id])->inRandomOrder()->limit(4)->get();
        foreach ($blog->recommended as $blogRecommended) {
            $blogRecommended->slug = route('blog_detail', ['slug'=>str_slug($blogRecommended->title, '-'), 'id'=>$blogRecommended->id]);
        }

        // get blog category
        $blogCategoryModel = new BlogCategory();
        $blogCategories = $blogCategoryModel->orderBy('category')->get();

        return view('blog_detail', compact('blog', 'blogCategories'));
    }
}
