<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coach;
use App\Course;

class CoachController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    //
    public function showAll(Request $request) {
        $coachModel = new Coach();

        $keyword = $request->get('keyword');
        if ($keyword != '') {
            $coachModel = $coachModel->where('name', 'like', '%'.$keyword.'%');
        }

        $coaches = $coachModel->orderBy('position')->paginate(24);

        foreach ($coaches as $coach) {
            $coach->slug = route('coach').'/'.str_slug($coach->name, '-').'-'.$coach->id.'.html';
        }

        return view('coach', compact('coaches'));
    }

    public function detail(Request $request, $slug, $id) {
        $coachModel = new Coach();
        $coach = $coachModel->findOrFail($id);

        $courseModel = new Course();
        $courses = $courseModel->orderBy('start_date', 'desc')->limit(3)->get();
        foreach ($courses as $course) {
            $course->slug = route('course').'/'.str_slug($course->title, '-').'-'.$course->id.'.html';
        }
        
        return view('coach_detail', compact('coach', 'courses'));
    }
}
