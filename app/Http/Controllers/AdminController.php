<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Validator;

use App\Banner;
use App\Blog;
use App\BlogCategory;
use App\Category;
use App\Coach;
use App\Course;
use App\CourseType;
use App\CourseCoach;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    // =============== Order ===============
    public function orderIndex() {
        return view('admin.order');
    }

    public function getOrders()
    {
        return DataTables::of(Order::query())
            ->addColumn('course_title', function (Order $order) {
                return $order->course->title;
            })
            ->addColumn('course_start', function (Order $order) {
                return Carbon::parse($order->course->start_date . $order->course->start_time);
            })
            ->addColumn('detail_url', function (Order $order) {
                return route('admin.orders.detail', $order->id);
            })
            ->addColumn('status', function (Order $order) {
                return $order->orderStatus->order_status;
            })
            ->toJson();
    }

    public function orderDetail(Request $request, $id) {
        $order = Order::findOrFail($id);
        if ($order->user_id != Auth::user()->id) {
            return redirect()->route('dashboard');
        }

        return view('admin.order_detail', compact('order'));
    }

    public function orderUpdate(Request $request, $id) {
        // Find order
        $order = Order::findOrFail($id);

        if ($order->order_status_id != 3 && $request->get('payment_confirmation') == true) {
            // payment confirmation
            $order->order_status_id = 3;
        }
        $order->save();

        return redirect()->route('admin.orders')->with('status', "Data pesanan ".$order->course->title." berhasil di simpan");
    }
    // =============== Order ===============

    // =============== Coach ===============
    public function coachIndex() {
        return view('admin.coach');
    }

    public function getCoaches()
    {
        return DataTables::of(Coach::query())
            ->addColumn('detail_url', function (Coach $coach) {
                return route('admin.coaches.detail', $coach->id);
            })
            ->toJson();
    }

    public function coachAddIndex() {
        return view('admin.coach_detail');
    }

    public function coachAdd(Request $request) {
        $this->validate($request,[
            'name' => 'required',
            'picture_url' => 'file|mimes:jpeg,png|max:3000',
        ]);  

        $picURL = '';
        if ($request->file('picture_url') != null) {
            $uploadedFile = $request->file('picture_url');
            $picURL = $uploadedFile->store('public/img/coach');
        }

        $coach = Coach::create([
            "name" => $request->get('name'),
            "highlight" => $request->get('highlight'),
            "description" => $request->get('description'),
            "picture_url" => $picURL,
            "address" => $request->get('address'),
            "phone" => $request->get('phone'),
            "email" => $request->get('email'),
            "facebook" => $request->get('facebook'),
            "twitter" => $request->get('twitter'),
            "instagram" => $request->get('instagram'),
        ]);

        return redirect()->route('admin.coaches')->with('status', "Data ".$coach->name." berhasil di simpan");
    }

    public function coachUpdate(Request $request, $id) {
        $this->validate($request,[
            'name' => 'required',
            'picture_url' => 'file|mimes:jpeg,png|max:3000',
        ]);

        $coach = Coach::findOrFail($id);
        
        $picURL = $coach->picture_url;
        if ($request->file('picture_url') != null) {
            $uploadedFile = $request->file('picture_url');
            $picURL = $uploadedFile->store('public/img/coach');
            if (!empty($coach->picture_url)) {
                // remove previous file
                Storage::delete($coach->picture_url);
            }
        }

        $coach->name = $request->get('name');
        $coach->highlight = $request->get('highlight');
        $coach->description = $request->get('description');
        $coach->picture_url = $picURL;
        $coach->address = $request->get('address');
        $coach->phone = $request->get('phone');
        $coach->email = $request->get('email');
        $coach->facebook = $request->get('facebook');
        $coach->twitter = $request->get('twitter');
        $coach->instagram = $request->get('instagram');
        $coach->save();

        return redirect()->route('admin.coaches')->with('status', "Data ".$coach->name." berhasil di simpan");
    }

    public function coachDetail(Request $request, $id) {
        $coach = Coach::findOrFail($id);
        return view('admin.coach_detail', compact('coach'));
    }

    public function coachDelete(Request $request, $id) {
        $coach = Coach::findOrFail($id);

        $coach->delete();
        return redirect()->route('admin.coaches')->with('status', "Data ".$coach->name." berhasil di delete");
    }
    // =============== Coach ===============

    // =============== Course ===============
    public function courseIndex() {
        return view('admin.course');
    }

    public function getCourses()
    {
        return DataTables::of(Course::query())
            ->addColumn('start', function (Course $course) {
                return Carbon::parse($course->start_date . $course->start_time);
            })
            ->addColumn('detail_url', function (Course $course) {
                return route('admin.courses.detail', $course->id);
            })
            ->toJson();
    }

    public function courseAddIndex() {
        $categories = Category::all();
        $coaches = Coach::all();
        $courseTypes = CourseType::all();
        return view('admin.course_detail', compact('categories', 'coaches', 'courseTypes'));
    }

    public function courseDetail(Request $request, $id) {
        $course = Course::findOrFail($id);
        $categories = Category::all();
        $coaches = Coach::all();
        $courseTypes = CourseType::all();
        $coachIDs = array();
        foreach ($course->coaches as $coach) {
            array_push($coachIDs, $coach->id);
        }
        return view('admin.course_detail', compact('course', 'categories', 'coaches', 'coachIDs', 'courseTypes'));
    }

    public function courseAdd(Request $request) {
        return $this->courseUpdate($request, 0);
    }

    public function courseUpdate(Request $request, $id) {
        $this->validate($request,[
            'title' => 'required',
            'banner_url' => 'file|mimes:jpeg,png|max:3000',
        ]);

        $course = new Course;
        if (!empty($id)){
            $course = Course::findOrFail($id);
        }

        $picURL = $course->banner_url;
        if ($request->file('banner_url') != null) {
            $uploadedFile = $request->file('banner_url');
            $picURL = $uploadedFile->store('public/img/course');
            if (!empty($course->banner_url)) {
                // remove previous file
                Storage::delete($course->banner_url);
            }
        }

        $course->title = $request->get('title');
        if (!empty($request->get('category_id'))) {
            $course->category_id = $request->get('category_id');
        }
        $course->description = $request->get('description');
        $course->banner_url = $picURL;
        $course->start_date = $request->get('start_date');
        $course->start_time = $request->get('start_time');
        $course->end_date = $request->get('end_date');
        $course->end_time = $request->get('end_time');
        $course->venue = $request->get('venue');
        $course->venue_address = $request->get('venue_address');
        $course->fee = $request->get('fee');
        $course->course_type_id = $request->get('course_type_id');
        $course->save();

        // delete previous coach and course relationships
        CourseCoach::where('course_id', $course->id)->delete();
        if (count($request->get('coaches')) > 0) {
            // add new coach and courde relationships
            foreach ($request->get('coaches') as $coachID) {
                $courseCoach = new CourseCoach;
                $courseCoach->course_id = $course->id;
                $courseCoach->coach_id = $coachID;
                $courseCoach->save();
            }
        }

        return redirect()->route('admin.courses')->with('status', "Data ".$course->title." berhasil di simpan");
    }

    public function courseDelete(Request $request, $id) {
        $course = Course::findOrFail($id);

        $course->delete();
        return redirect()->route('admin.courses')->with('status', "Data ".$course->title." berhasil di delete");
    }


// =============== CATEGORY ===============
    public function categoryIndex() {
        return view('admin.category');
    }

    public function getCategories()
    {
        return DataTables::of(Category::query())
            ->addColumn('detail_url', function (Category $category) {
                return route('admin.categories.detail', $category->id);
            })
            ->toJson();
    }

    public function categoryAddIndex() {
        return view('admin.category_detail');
    }

    public function categoryAdd(Request $request) {
        $attributes = [
            'category' => 'Kategori',
        ];
        
        $validator = Validator::make($request->all(), [
            'category' => 'required|unique:categories',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $category = Category::create([
            "category" => $request->get('category'),
        ]);

        return redirect()->route('admin.categories')->with('status', "Data ".$category->category." berhasil di simpan");
    }

    public function categoryUpdate(Request $request, $id) {
        $attributes = [
            'category' => 'Kategori',
        ];
        
        $validator = Validator::make($request->all(), [
            'category' => 'required|unique:categories',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $category = Category::findOrFail($id);

        $category->category = $request->get('category');
        $category->save();

        return redirect()->route('admin.categories')->with('status', "Data ".$category->category." berhasil di simpan");
    }

    public function categoryDetail(Request $request, $id) {
        $category = Category::findOrFail($id);
        return view('admin.category_detail', compact('category'));
    }

    public function categoryDelete(Request $request, $id) {
        $category = Category::findOrFail($id);

        Course::where('category_id', $id)->update(['category_id'=>null]);

        $category->delete();
        return redirect()->route('admin.categories')->with('status', "Data ".$category->category." berhasil di delete");
    }

// =============== CATEGORY ===============

// =============== BANNER ===============
    public function bannerIndex() {
        return view('admin.banner');
    }

    public function getBanners()
    {
        return DataTables::of(Banner::query())
            ->addColumn('desktop_banner_url', function (Banner $banner) {
                return Storage::url($banner->desktop_banner);
            })
            ->addColumn('detail_url', function (Banner $banner) {
                return route('admin.banners.detail', $banner->id);
            })
            ->toJson();
    }

    public function bannerAddIndex() {
        return view('admin.banner_detail');
    }

    public function bannerAdd(Request $request) {
        $attributes = [
            'name' => 'Nama',
            'position' => 'Posisi',
        ];
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'desktop_banner' => 'file|mimes:jpeg,png|max:3000',
            'mobile_banner' => 'file|mimes:jpeg,png|max:3000',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $desktopBanner = '';
        $mobileBanner = '';
        if ($request->file('desktop_banner') != null) {
            $uploadedFile = $request->file('desktop_banner');
            $desktopBanner = $uploadedFile->store('public/img/banner');
        }
        if ($request->file('mobile_banner') != null) {
            $uploadedFile = $request->file('mobile_banner');
            $mobileBanner = $uploadedFile->store('public/img/banner');
        }

        $banner = Banner::create([
            "name" => $request->get('name'),
            "desktop_banner" => $desktopBanner,
            "mobile_banner" => $mobileBanner,
            "url" => $request->get('url'),
            "position" => $request->get('position'),
            "fg_active" => $request->get('fg_active'),
        ]);

        return redirect()->route('admin.banners')->with('status', "Data ".$banner->name." berhasil di simpan");
    }

    public function bannerUpdate(Request $request, $id) {
        $attributes = [
            'name' => 'Nama',
            'position' => 'Posisi',
        ];
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'desktop_banner' => 'file|mimes:jpeg,png|max:3000',
            'mobile_banner' => 'file|mimes:jpeg,png|max:3000',
            'position' => 'numeric',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $banner = Banner::findOrFail($id);

        $banner->name = $request->get('name');
        if ($request->file('desktop_banner') != null) {
            $uploadedFile = $request->file('desktop_banner');
            $path = $uploadedFile->store('public/img/banner');
            
            if (!empty($banner->desktop_banner)) {
                // remove previous file
                Storage::delete($banner->desktop_banner);
            }

            // save path
            $banner->desktop_banner = $path;
        }
        if ($request->file('mobile_banner') != null) {
            $uploadedFile = $request->file('mobile_banner');
            $path = $uploadedFile->store('public/img/banner');
            
            if (!empty($banner->mobile_banner)) {
                // remove previous file
                Storage::delete($banner->mobile_banner);
            }

            // save path
            $banner->mobile_banner = $path;
        }
        $banner->url = $request->get('url');
        $banner->position = $request->get('position');
        $banner->fg_active = $request->get('fg_active');
        $banner->save();

        return redirect()->route('admin.banners')->with('status', "Data ".$banner->name." berhasil di simpan");
    }

    public function bannerDetail(Request $request, $id) {
        $banner = Banner::findOrFail($id);
        return view('admin.banner_detail', compact('banner'));
    }

    public function bannerDelete(Request $request, $id) {
        $banner = Banner::findOrFail($id);
        
        if (!empty($banner->desktop_banner)) {
            // remove previous file
            Storage::delete($banner->desktop_banner);
        }
        if (!empty($banner->mobile_banner)) {
            // remove previous file
            Storage::delete($banner->mobile_banner);
        }

        $banner->delete();
        return redirect()->route('admin.banners')->with('status', "Data ".$banner->name." berhasil di delete");
    }

    // =============== BANNER ===============

    // =============== BLOG ===============
    public function blogIndex() {
        return view('admin.blog');
    }

    public function getBlogs()
    {
        return DataTables::of(Blog::query())
            ->addColumn('detail_url', function (Blog $blog) {
                return route('admin.blogs.detail', $blog->id);
            })
            ->toJson();
    }

    public function blogAddIndex() {
        $coaches = Coach::all();
        $blogCategories = BlogCategory::all();
        return view('admin.blog_detail', compact('coaches', 'blogCategories'));
    }

    public function blogAdd(Request $request) {
        $attributes = [
            'title' => 'Judul',
        ];
        
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'banner_url' => 'file|mimes:jpeg,png|max:3000',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $bannerURL = '';
        if ($request->file('banner_url') != null) {
            $uploadedFile = $request->file('banner_url');
            $bannerURL = $uploadedFile->store('public/img/banner');
        }

        $blog = new Blog();
        $blog->banner_url = $bannerURL;
        $blog->title = $request->get('title');
        $blog->highlight = $request->get('highlight');
        $blog->body = $request->get('body');
        $blog->coach_id = $request->get('coach_id');
        if (empty($request->get('coach_id'))) {
            $blog->coach_id = null;
        }
        $blog->blog_category_id = $request->get('blog_category_id');
        if (empty($request->get('blog_category_id'))) {
            $blog->blog_category_id = null;
        }
        $blog->save();

        return redirect()->route('admin.blogs')->with('status', "Data ".$blog->title." berhasil di simpan");
    }

    public function blogUpdate(Request $request, $id) {
        $attributes = [
            'title' => 'Judul',
        ];
        
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'banner_url' => 'file|mimes:jpeg,png|max:3000',
        ], [], $attributes);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $blog = Blog::findOrFail($id);

        if ($request->file('banner_url') != null) {
            $uploadedFile = $request->file('banner_url');
            $path = $uploadedFile->store('public/img/banner');
            
            if (!empty($blog->banner_url)) {
                // remove previous file
                Storage::delete($blog->banner_url);
            }

            // save path
            $blog->banner_url = $path;
        }
        $blog->title = $request->get('title');
        $blog->highlight = $request->get('highlight');
        $blog->body = $request->get('body');
        $blog->coach_id = $request->get('coach_id');
        if (empty($request->get('coach_id'))) {
            $blog->coach_id = null;
        }
        $blog->blog_category_id = $request->get('blog_category_id');
        if (empty($request->get('blog_category_id'))) {
            $blog->blog_category_id = null;
        }
        $blog->save();

        return redirect()->route('admin.blogs')->with('status', "Data ".$blog->title." berhasil di simpan");
    }

    public function blogDetail(Request $request, $id) {
        $blog = Blog::findOrFail($id);

        $coaches = Coach::all();
        $blogCategories = BlogCategory::all();

        return view('admin.blog_detail', compact('blog', 'coaches', 'blogCategories'));
    }

    public function blogDelete(Request $request, $id) {
        $blog = Blog::findOrFail($id);
        
        if (!empty($blog->banner_url)) {
            // remove previous file
            Storage::delete($blog->banner_url);
        }

        $blog->delete();
        return redirect()->route('admin.blogs')->with('status', "Data ".$blog->title." berhasil di delete");
    }

    // =============== BLOG ===============

}