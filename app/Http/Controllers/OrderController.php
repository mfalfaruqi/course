<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Order;
use App\Cart;
use App\Course;

class OrderController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function checkout(Request $request) {
        $userID = Auth::user()->id;

        $carts = Cart::where('user_id', $userID)->with('course.category')->get();

        return view('checkout', compact('carts'));
    }

    public function create(Request $request) {
        $userID = Auth::user()->id;
        
        // get cart
        $carts = Cart::where('user_id', $userID)->get();
        if ($carts->isEmpty()) {
            return redirect()->route('checkout');
        }

        foreach ($carts as $cart) {
            $course = Course::findOrFail($cart->course_id);

            $order = new Order;
            $order->invoice_price = $course->fee;
            $order->course_id = $course->id;
            $order->user_id = $userID;
            $order->order_status_id = 1;
            $order->save();

            $order->order_number = 'INV/'. Carbon::now()->format('Ymd')."/" . str_pad($order->id, 10, '0', STR_PAD_LEFT);
            $order->save();
        }

        // remove cart
        Cart::where('user_id', $userID)->delete();
        $order->course->slug = route('course').'/'.str_slug($order->course->title, '-').'-'.$order->course->id.'.html';

        return view('thankyou', compact('order'));
    }

    public function addToCart(Request $request) {
        $userID = Auth::user()->id;
        $courseID = $request->get('course_id');

        // remove previous cart
        Cart::where('user_id', $userID)->delete();

        $cart = new Cart;
        $cart->user_id = $userID;
        $cart->course_id = $courseID;
        $cart->save();

        return redirect()->route('checkout');
    }
}
