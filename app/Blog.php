<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function category() {
        return $this->belongsTo('App\BlogCategory', 'blog_category_id');
    }

    public function coach() {
        return $this->belongsTo('App\Coach');
    }
}
