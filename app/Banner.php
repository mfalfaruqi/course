<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $fillable = ['name', 'desktop_banner', 'mobile_banner', 'url', 'position', 'fg_active'];
}
