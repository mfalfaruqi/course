const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass-genius/style.scss', 'public/css')
   .sass('resources/sass-login/login.scss', 'public/css')
   .sass('resources/sass/custom.scss', 'public/css')
   .js('resources/js/login.js', 'public/js')
   .js('resources/js/custom.js', 'public/js')
   // .styles(['resources/css/test.css'], 'public/css/test.css')
   .version();
